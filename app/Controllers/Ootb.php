<?php

namespace App\Controllers;

use CodeIgniter\Controller;

class Ootb extends Controller
{
  
    public function index(){
        echo "Read the 'README.md' file from document root";
        echo "";
        echo "Happy coding. =)";
    }

}
