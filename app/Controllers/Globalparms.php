<?php namespace App\Controllers;

class Globalparms extends BaseCrud
{
  public function index()
   {
    $this->permitido=array(2,16);
    $this->seguridad();
    $crud = $this->_getGroceryCrudEnterprise();
    $crud->setTable('GLOBAL_PARMS')
         ->setSubject('Entrada', 'Parámetros Globales')
         ->columns(['parm','value','notes'])
         ->fields(['parm','value','notes'])
         ->requiredFields(['parm','value'])
         ;
    $output = $crud->render();
    return $this->output($output);
   }
}