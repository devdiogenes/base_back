<?php namespace App\Controllers;

class Landing extends BaseCrud
{
  public function index()
   {
    $this->permitido=array(2,16);
    $this->seguridad();
    $crud = $this->_getGroceryCrudEnterprise();
    $crud->setTable('LANDING')
         ->setSubject('Landing', 'Landing')
         ->columns(['name','email','token','company','domain','comment'])
         ->fields(['name','email','token','company','domain','comment'])
         ->requiredFields(['name','email'])
         ;
    $output = $crud->render();
    return $this->output($output);
   }
}