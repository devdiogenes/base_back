<?php namespace App\Controllers;

class Plans extends BaseCrud
{
  public function index()
   {
    $this->permitido=array(2,16);
    $this->seguridad();
    $crud = $this->_getGroceryCrudEnterprise();
    $crud->setTable('PLANS')
         ->setSubject('Plan', 'Planes')
         ->columns(['name'])
         ->fields(['name'])
         ->requiredFields(['name']);
    $output = $crud->render();
    return $this->output($output);
   }
}