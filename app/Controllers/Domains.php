<?php namespace App\Controllers;

class Domains extends BaseCrud
{
  public function index()
   {
    $this->permitido=array(2,16);
    $this->seguridad();
    $crud = $this->_getGroceryCrudEnterprise();
    $crud->setTable('REL_USERS_PLANS')
         ->setSubject('Dominio', 'Dominios')
         ->columns(['id_user','id_plan','domain','date_ini','date_end','remaining'])
         ->fields(['id_user','id_plan','domain','date_ini','date_end','remaining'])
         ->requiredFields(['id_user','id_plan','domain','date_ini','date_end','remaining'])
         ->setRelation('id_user','USERS','name')
         ->setRelation('id_plan','PLANS','name');
    $output = $crud->render();
    return $this->output($output);
   }
}