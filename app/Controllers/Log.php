<?php namespace App\Controllers;

class Log extends BaseCrud
{
  public function index()
   {
    $this->permitido=array(2,16);
    $this->seguridad();
    $crud = $this->_getGroceryCrudEnterprise();
    $crud->setTable('USER_LOG')
         ->setSubject('Entrada', 'Entradas')
         ->columns(['id_user','action','date_action','ip_user'])
         ->fields(['id_user','action','date_action','ip_user'])
         ->requiredFields(['id_user','action','date_action','ip_user'])
         ->setRelation('id_user','USERS','name');
    $output = $crud->render();
    return $this->output($output);
   }
}