<?php namespace App\Controllers;

class Users extends BaseCrud
{
  public function index()
   {
    $this->permitido=array(2,16);
    $this->seguridad();
    $crud = $this->_getGroceryCrudEnterprise();
    $crud->setTable('USERS')
         ->setSubject('Usuario', 'Usuarios')
         ->columns(['name','password','email','language'])
         ->fields(['name','password','email','language','permisos'])
         ->requiredFields(['name','password','email','language'])
         ->setRelationNtoN('permisos','REL_USERS_CATEGORIES','USERS_CATEGORIES','id_user','id_category','name');
    $output = $crud->render();
    return $this->output($output);
   }
}