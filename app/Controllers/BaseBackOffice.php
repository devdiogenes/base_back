<?php
//bbo_users_plans
//bbo_user_log

namespace App\Controllers;

include(APPPATH . 'Libraries/GroceryCrudEnterprise/autoload.php');

use GroceryCrud\Core\GroceryCrud;

class BaseBackOffice extends BaseControllerCrud
{



	public function bbo_login_check()
	{
		if (isset($_POST['password'])) {
			$this->bbo_login();
		}
		return $this->user->loged ? $this->_render("/bbo/body") : $this->_render("/bbo/body_login", [], False);
	}

	private function bbo_login()
	{
		$this->user->login(\md5($_POST['user'] . $_POST['password']));
	}

	public function bbo_logout()
	{
		$this->user->logout();
		return $this->_render("/bbo/body_login", [], False);
	}

	public function _render($view_body = "", $view_data = [], $load_menu = True)
	{

		//https://codeigniter4.github.io/userguide/outgoing/views.html#loading-multiple-views
		return $load_menu ?
			view('/bbo/header') . view('/bbo/menu_left') . view($view_body, $view_data) . view('/bbo/footer') :
			view('/bbo/header') . view($view_body, $view_data) . view('/bbo/footer');
	}

	public function bbo_global_params()
	{
		$crud = $this->_getGroceryCrudEnterprise();
		$crud->setTable('GLOBAL_PARMS');
		$crud->setSubject('GLOBAL_PARMS', 'GLOBAL PARMS');
		$crud->addFields(['parm', 'value', 'notes']);
		$crud->editFields(['parm', 'value', 'notes']);
		$crud->columns(['parm', 'value', 'notes']);

		$output = $crud->render();
		return $this->output($output);
	}

	public function bbo_landing()
	{
		$crud = $this->_getGroceryCrudEnterprise();
		$crud->setTable('LANDING');
		$crud->setSubject('LANDING', 'LANDING VALUE');
		$crud->columns(['name', 'email', 'token', 'company', 'domain', 'comment']);

		$output = $crud->render();
		return $this->output($output);
	}

	public function bbo_languages()
	{
		$crud = $this->_getGroceryCrudEnterprise();
		$crud->setTable('LANGUAGES');
		$crud->setSubject('Language', 'Language');
		$crud->columns(['id_language', 'language']);

		$output = $crud->render();
		return $this->output($output);
	}

	public function bbo_plans()
	{
		$crud = $this->_getGroceryCrudEnterprise();
		$crud->setTable('PLANS');
		$crud->setSubject('Plans', 'Plans');
		$crud->columns(['id_plan', 'name', 'stripe_id']);

		$output = $crud->render();
		return $this->output($output);
	}

	public function bbo_categories()
	{
		$crud = $this->_getGroceryCrudEnterprise();
		$crud->setTable('USERS_CATEGORIES');
		$crud->setSubject('User', 'User Categories');
		$crud->columns(['id_user_category', 'name', 'level']);

		$output = $crud->render();
		return $this->output($output);
	}

	public function bbo_rel_users_categories()
	{
		$crud = $this->_getGroceryCrudEnterprise();
		$crud->setTable('REL_USERS_CATEGORIES');
		$crud->setSubject('Rel_User_Categories', 'Rel User Categories');
		$crud->columns(['id_ruc', 'id_user', 'id_category']);
		$crud->displayAs('id_user', 'User');
		$crud->displayAs('id_category', 'Category');
		$crud->setRelation('id_user', 'USERS', 'name');
		$crud->setRelation('id_category', 'USERS_CATEGORIES', 'name');
		$output = $crud->render();
		return $this->output($output);
	}

	public function bbo_user_log()
	{
		$crud = $this->_getGroceryCrudEnterprise();
		$crud->setTable('USER_LOG');
		$crud->setSubject('UserLog', 'User Log');
		$crud->columns(['id_user_log', 'action', 'date_action', 'id_user', 'ip_user']);
		$crud->setRelation('id_user', 'USERS', 'name');

		$output = $crud->render();
		return $this->output($output);
	}

	public function bbo_user()
	{
		$crud = $this->_getGroceryCrudEnterprise();
		$crud->setTable('USERS');
		$crud->setSubject('Users', 'Users');
		$crud->columns(['id_user', 'name', 'password', 'email', 'date_created', 'language', 'company', 'phone', 'checked', 'comercial_allowed']);
		$crud->setRelationNtoN('Plan', 'REL_USERS_PLANS', 'PLANS', 'id_user', 'id_plan', 'name' );
		$crud->setRelationNtoN('Category', 'REL_USERS_CATEGORIES', 'USERS_CATEGORIES', 'id_user', 'id_category', 'name' );
		$output = $crud->render();
		return $this->output($output);
	}

	public function bbo_rel_users_plans()
	{
		$crud = $this->_getGroceryCrudEnterprise();
		$crud->setTable('REL_USERS_PLANS');
		$crud->setSubject('Rel_Users_plan', 'Rel Users PLan');
		$crud->columns(['id_rup', 'id_user', 'id_plan', 'domain', 'date_ini', 'date_end', 'remaining']);
		$crud->displayAs('id_rup', 'ID');
		$crud->displayAs('id_user', 'User');
		$crud->displayAs('id_plan', 'Plan');
		$crud->setRelation('id_user', 'USERS', 'name');
		$crud->setRelation('id_plan', 'PLANS', 'name');

		$output = $crud->render();
		return $this->output($output);
	}

	protected function _render_crud($output = NULL)
	{
		echo "HOLA";
		if (isset($output->isJSONResponse) && $output->isJSONResponse) {
			header('Content-Type: application/json; charset=utf-8');
			echo $output->output;
			exit;
		}
		echo "HOLA";
		return $this->output('/Gc', ["output" => $output]);
	}
}
