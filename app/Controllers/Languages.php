<?php namespace App\Controllers;

class Languages extends BaseCrud
{
  public function index()
   {
    $this->permitido=array(2,16);
    $this->seguridad();
    $crud = $this->_getGroceryCrudEnterprise();
    $crud->setTable('LANGUAGES')
         ->setSubject('Idioma', 'Idiomas')
         ->columns(['id_language', 'language'])
         ->fields(['id_language', 'language'])
         ->requiredFields(['id_language', 'language']);
    $output = $crud->render();
    return $this->output($output);
   }
}