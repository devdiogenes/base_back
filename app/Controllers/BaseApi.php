<?php

namespace App\Controllers;

class BaseApi extends BaseController
{

  public $permitido;            // permitidos todos los niveles de seguridad
  protected $gp;
  protected $siteUrl;
  protected $request;
  protected $verbose = false;   // 
  //initController: set up json response headers
  public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
   {
    parent::initController($request, $response, $logger);
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
    header("Access-Control-Allow-Methods: POST");
    $this->gp = new \App\Models\Global_parms();
    $this->verbose = ($this->gp->getParm('api.verbose')->value == 'true');
    $this->siteUrl = $this->gp->getParm('siteUrl')->value;

    $this->request = $request = \Config\Services::request();
   }

  ////////////////////////////////// AutoTests ///////////////////////////////////////////////////
  public function index(){
    $this->are_you_alive();
  }
  public function are_you_alive()
   {
    $this->send_response("yes");
   }
  ////////////////////////////////// Base Actions ///////////////////////////////////////////////////
  public function Bland($vista = 'Bland')
   {
    $land = new \App\Models\Land();
    $varlist = array();
    foreach ($land->allowedFields as $field) 
    {
      if (isset($_POST[$field]))
        $varlist[$field] = $_POST[$field];
      else 
      {
        $this->user->log("Failed Landing: no $field provided");
        $this->error_send(400);
      }
    }
    $land->save($varlist);
    $this->user->log("{$varlist['name']}: Landed");
    $email = \Config\Services::email();
    $this->configMail($email);
    $email->setFrom($this->gp->getParm('base.FromMail')->value);
    $email->setTo($this->gp->getParm('base.LandSendMail')->value);
    $email->setSubject('User landed.');
    $email->setMessage(view($vista, $varlist));
    if (!$email->send())
      $this->error_send(503, $email->printDebugger());
    $this->send_response('User landed.');
   }
  public function Blogin()
   {
    $this->Alogin();
   }
  public function Blogout()
   {
    $this->Alogout();
   }
  public function Alogin($token = '')
   {
     
    if ($token == '')
      if (isset($_POST['token']))
        $token = $_POST['token'];
      else {
        $this->user->log("Failed Login attempt: no token provided");
        $this->error_send(400);
      }
    if (strlen($token) != 32) {
      $this->user->log("Failed Login attempt: invalid token $token");
      $this->error_send(406);
      
    }
    
    $id_user = $this->user->login($token);
    if ($id_user !== true) {
      $this->user->log("Failed Login attempt: not found $token");
      $this->error_send(404);
    }
    $this->user->log("Loged user {$this->user->userdata->name}");
    $this->send_response(array());
  
   }
  public function Alogout()
   {

    if ($this->user->loged) {
      $this->user->logout();
      $this->user->log('Logout user ' . $this->user->userdata->name);
    } else
      $this->user->log('Logout user <not loged>');
    echo json_encode('ok');
    die();
   }
  public function BgetUserPlans()
   {
    $this->seguridad(array(0));
    $plan = new \App\Models\Plan();
    $this->send_response($plan->getUserPlans($this->user->id));
    if ($this->verbose)
      $this->user->log("{$this->user->userdata->name}: User plans obtained.");
   }
  public function BuserUpdate() // data user update
   {
    $this->seguridad(array(0));
    $userdata = $this->getFormUser(); // retrieves POST vars. Error if any not provided
    if ($this->user->update($this->user->id, $userdata))  // update and error test
    {
      $this->send_response($this->user->find($this->user->id));
      if ($this->verbose)
        $this->user->log("{$this->user->userdata->name}: data user updated.");
    } else {
      $this->error_send(400);
      
      if ($this->verbose)
        $this->user->log("{$this->user->userdata->name}: data user update error.");
    }
   }
  public function BuserRegister($vista='BuserRegister')
   {
    // intencionadamente, no se llama a seguridad, ya que no tiene por que estar dado de alta el usuario
    $userdata = $this->getFormUser(); // retrieves POST vars. Error if any not provided

    if ($this->user->checkMail($userdata['email']))
    {
      $this->user->insert($userdata);         // if user does not exists, insert into database
      $this->sendMailActivation($userdata,$vista);   // and send activation mail
      if ($this->verbose)
        $this->user->log("{$userdata['name']}: new user registration.");
      $this->send_response("Registration ok. Awaiting user activation.");
    } else {
      $this->error_send(406);
      if ($this->verbose)
        $this->user->log("{$userdata['name']}: new user register error.");
    }
   }
  public function BpasswordReminder($vista='BpasswordReminder')
   {
    if (!isset($_POST['email']))
    $this->error_send(400);

    $email = $_POST['email'];
    $password = $this->user->getPasswordFromMail($email);
    if (!$password) 
      $this->error_send(404);
 
    $this->sendPassword($email,$vista);
   }
  public function BuserActivate($key) // user activation
   {
    $vista = 'BuserActivate';
    $varlist = (array)$this->user->activate($key);
    if ($this->verbose)
      $this->user->log("{$key} activated");
    $location = $this->siteUrl . '/#/Login';
    header("Location: $location");
    echo " Redirigiendo a $location. Si este mensaje no desaparece automáticamente, <a href='$location'> haga click aquí</a>. ";
    $email = \Config\Services::email();
    $this->configMail($email);
    $email->setFrom($this->gp->getParm('base.FromMail')->value);
    $email->setTo($this->gp->getParm('base.UserActivatedSendMail')->value);
    $email->setMessage(view($vista, $varlist));
    $email->setSubject('User activated.');
    if (!$email->send())
      $this->error_send(503, $email->printDebugger());
    die();
   }
  public function BgetPlanList()   // retrieves all availlable plans
   {
    $plan = new \App\Models\Plan();
    if ($this->verbose and $this->user->loged)
      $this->user->log("{$this->user->userdata->name}: plan list obtained.");
    $this->send_response($plan->findAll());
   }
  public function BgetPlan()   // retrieves plan details
   {
    if (isset($_POST['id_plan']))
      $id_plan = $_POST['id_plan'];
    else {
      $this->user->log("Failed BgetPlan: no id_plan provided");
      $this->error_send(400);
    }
    $plan = new \App\Models\Plan();
    if ($this->verbose  and $this->user->loged)
      $this->user->log("{$this->user->userdata->name}: plan $id_plan info obtained.");
    $this->send_response($plan->find($id_plan));
   }
  private function BsuscribePlan($notifyMail = true)
   {
    $vista = 'BdomainInsert';  // change this for using a view
    $vistaUser = 'BdomainInsertUser';  // this one, for the view to be sent to the user.
    $this->seguridad(array(0));      // user need to be loged. 
    $dataPlan = array('id_user' => $this->user->id);
    $userPlan = new \App\Models\UserPlan();
    if (isset($_POST['id_plan']))
      $dataPlan['id_plan'] = $_POST['id_plan'];
    else
      $dataPlan['id_plan'] =1;
    $plan = new \App\Models\Plan();
    $varlist['plan'] =  $plan->find($dataPlan['id_plan']);
    $varlist['user'] = $this->user->userdata;
    $dataPlan['planName'] = $varlist['plan']->name;  
    if (isset($_POST['domain']))
      $dataPlan['domain'] = $_POST['domain'];
    else
     {
      $this->user->log("Failed BsuscribePlan: no domain provided");
      $this->error_send(400);
     }
    if (isset($_POST['date_end']))
      $dataPlan['date_end'] = $_POST['date_end'];
    if (isset($_POST['remaining']))
      $dataPlan['remaining'] = $_POST['remaining'];
    $varlist['domain'] = $dataPlan['domain'];
    $this->user->log("BsuscribePlan: user {$this->user->userdata->name}, plan {$dataPlan['id_plan']}.");
    $userPlan->insert($dataPlan);
    $dataPlan['id_rup'] = $userPlan->insertID(); 
    $dataPlan['token'] = md5("{$dataPlan['id_rup']}{$dataPlan['domain']}");
    if($notifyMail)
    {
     $email = \Config\Services::email();
     $this->configMail($email);
     $email->setFrom($this->gp->getParm('base.FromMail')->value);
     $email->setTo($this->gp->getParm('base.SuscribeSendMail')->value);
     $email->setSubject('Plan suscrito.');
     $email->setMessage(view($vista, $varlist));
     if (!$email->send())
       $this->error_send(503, $email->printDebugger());
     // send an email for the user
     $email->setFrom($this->gp->getParm('base.FromMail')->value);
     $email->setTo($varlist['user']->email);
     $email->setSubject('Plan suscrito.');
     $email->setMessage(view($vistaUser, $varlist));
     if (!$email->send())
       $this->error_send(503, $email->printDebugger());
    }

    $this->send_response($dataPlan);
   }
  private function BupdatePlan($notifyMail = true)
   {
    $vista = 'BdomainUpdate';  // change this for using a view
    $vistaUser = 'BdomainUpdateUser';  // this one, for the view to be sent to the user.
    $this->seguridad(array(0));      // user need to be loged. 
    $dataPlan = array('id_user' => $this->user->id);
    $userPlan = new \App\Models\UserPlan();
    if (isset($_POST['id_rup']))
      $id_rup = $_POST['id_rup'];
    else 
     {
      $this->user->log("Failed BupdatePlan: no id_rup provided");
      $this->error_send(400);
     }
    $dataPlan = (array)$userPlan->find($id_rup);
    if (isset($_POST['domain']))
      $dataPlan['domain'] = $_POST['domain'];
    if (isset($_POST['id_plan']))
      $dataPlan['id_plan'] = $_POST['id_plan'];
    if (isset($_POST['date_end']))
      $dataPlan['date_end'] = $_POST['date_end'];
    if (isset($_POST['remaining']))
      $dataPlan['remaining'] = $_POST['remaining'];
    $plan = new \App\Models\Plan();
    $varlist['plan'] = $plan->find($dataPlan['id_plan']);
    $varlist['user'] = $this->user->userdata;
    $dataPlan['planName'] = $varlist['plan']->name; 
    if (!isset($_POST['date_end']))
      $dataPlan['date_end'] = 0;
    if (isset($_POST['remaining']))
      $dataPlan['remaining'] = $_POST['remaining'];
    $varlist['domain'] = $dataPlan['domain'];
    $this->user->log("BupdatePlan: user {$this->user->userdata->name}, plan {$dataPlan['id_plan']}.");
    $userPlan->save($dataPlan);
    $dataPlan['token'] = md5("{$dataPlan['id_rup']}{$dataPlan['domain']}");
    if($notifyMail)
    {
     $email = \Config\Services::email();
     $this->configMail($email);
     $email->setFrom($this->gp->getParm('base.FromMail')->value);
     $email->setTo($this->gp->getParm('base.SuscribeSendMail')->value);
     $email->setSubject('Plan modificado.');
     $email->setMessage(view($vista, $varlist));
     if (!$email->send())
       $this->error_send(503, $email->printDebugger());
     // send an email for the user
     $email->setFrom($this->gp->getParm('base.FromMail')->value);
     $email->setTo($varlist['user']->email);
     $email->setSubject('Plan modificado.');
     $email->setMessage(view($vistaUser, $varlist));
     if (!$email->send())
       $this->error_send(503, $email->printDebugger());
    }
    $this->send_response($dataPlan);
   }
  public function BdomainInsert()
   {
     $this->BsuscribePlan();
   }
  public function BdomainInsertNoMail()
   {
    $this->BsuscribePlan(false);
   }
  public function BdomainUpdate()
   {
     $this->BupdatePlan();
   }
  public function BdomainUpdateNoMail()
   {
     $this->BupdatePlan(false);
   }
  public function BPasswordSave()
   {
     $this->BpasswordRecover();
   }
  public function BpasswordRecover()
   {
    if (isset($_POST['parm']))
      $md5email = $_POST['parm'];
    else {
      $this->user->log("Failed to recover password. Param not provided");
      $this->error_send(400);
    }
    if (isset($_POST['password']))
      $password = $_POST['password'];
    else {
      $this->user->log("Failed to recover password. Password not provided");
      $this->error_send(400);
    }
    $this->user->setPasswordFromEncodedMail($md5email, $password);
    $this->send_response('ok');
   }
  /////////////////////////////////////// Private functions ////////////////////////////////////////////
  protected function sendMailActivation($userdata,$vista)
   {
    $key = md5($userdata['name'] . $userdata['email'] . $userdata['password']);
    $link = base_url() . "/Api/BuserActivate/$key";
    $userdata['link'] = $link;
    $email = \Config\Services::email();
    $this->configMail($email);
    $email->setFrom('notifications@cometwise.com', "Don't reply this mail.");
    $email->setTo($userdata['email']);
    $email->setSubject('User activation.');
    $email->setMessage(view($vista, $userdata));
    if (!$email->send())
      $this->send_response(array(
        'Code'      => 500,
        'error'     => 'Fallo en el envío de correo',
        'details'   => $email->printDebugger()
      ));
   }
  protected function configMail($email)
   {
    $Parms = new \App\Models\Global_parms();
    $config['SMTPPass'] = $Parms->getParm('base.SMTPPass')->value;
    $config['SMTPPort'] = $Parms->getParm('base.SMTPPort')->value;;
    $config['SMTPHost'] = $Parms->getParm('base.SMTPHost')->value;;
    $config['SMTPUser'] = $Parms->getParm('base.SMTPUser')->value;;
    $email->initialize($config);
   }
  protected function sendPassword($usermail, $vista, $dataview = array())
   {
    $md5email = md5($usermail);
    $dataview['md5email'] = $md5email ;
    $dataview['siteUrl'] = $this->siteUrl;
    $email = \Config\Services::email();
    $this->configMail($email);
    $email->setFrom('notifications@cometwise.com', "Don't reply this mail.");
    $email->setTo($usermail);
    $email->setSubject('Password recovery.');
    if ($vista == '')  // default message
      $email->setMessage("
       <html><body>
        Ha sido solicitado el restablecimiento de su contraseña en CometWise. Si no fue Vd. quien lo solicitó, simplemente ignore este mensaje.<br>
        <a href='{$this->siteUrl}/#/PasswordReestablish?parm=$md5email'> Haga click en este enlace para restablecer la contraseña. </a>
      </body></html>");
    else
      $email->setMessage(view($vista, $dataview));
    if (!$email->send())
      $this->error_send(503, $email->printDebugger());

    $this->send_response('Password sent.');
   }
  protected function getFormUser()
   {
    $userdata = array();
    if (isset($_POST['name']))
      $userdata['name'] = $_POST['name'];
    else {
      $this->user->log("Failed update user: no name provided");
      $this->error_send(400);
      
    }
    if (isset($_POST['password']))
      $userdata['password'] = $_POST['password'];
    if (isset($_POST['email']))
      $userdata['email'] = $_POST['email'];
    else {
      $this->user->log("Failed update user: no email provided");
      $this->error_send(400);
    }
    if (isset($_POST['language']))
      $userdata['language'] = $_POST['language'];
    else {
      $this->user->log("Failed update user: no language provided");
      $this->error_send(400);
    }
    if (isset($_POST['company']))
      $userdata['company'] = $_POST['company'];
    else {
      $this->user->log("Failed update user: no company provided");
      $this->error_send(400);
    }
    if (isset($_POST['phone']))
      $userdata['phone'] = $_POST['phone'];
    else {
      $this->user->log("Failed update user: no phone provided");
      $this->error_send(400);
    }
    if (isset($_POST['comercial_allowed']))
      $userdata['comercial_allowed'] = $_POST['comercial_allowed'];
    return $userdata;
   }
  protected function send_response($response, $error = 0)       // general output. Format and sends json and die.
   {
    
    if ($error)                                               // if error, set the header
    {
      header("HTTP/1.1 {$response['code']} {$response['error']}");
      echo json_encode($response, JSON_FORCE_OBJECT);       // and the data
      die();
    }
    // Output compound
    $diff15Minutes = new \DateInterval('PT15M');
    $d0 = new \DateTime(date('Y/m/d G:i:s'));
    $d0->add($diff15Minutes);
    $exp = $d0->format('Y/m/d G:i:s');
    $output = array(
      'dataUser' => $this->user->userdata,
      'timeout'  => $exp,
      'payload'  => $response,
    );

    echo json_encode($output);
    die();
   }
  protected function error_send($code, $details = '')
   {
    //https://suments.atlassian.net/wiki/spaces/DIOG/pages/867041281/Base+Back#Servicios.-Input%2C-output-%26-error
    $this->send_response(array(
          'code'        => $code,
          'error'        => $this->error_message_get($code),
          'details'      => $details
        ), true);
   }

  protected function error_message_get($code)
   {
    //https://suments.atlassian.net/wiki/spaces/DIOG/pages/867041281/Base+Back#Servicios.-Input%2C-output-%26-error
    $msgs = array(
      400 => "Missing data",
      404 => "Not found",
      406 => "Data not accepted",
      498 => "",
      503 => "Service unavailable"
    );
    return $msgs[$code];
   }
  protected function allow($perm)   // test for permissions
   {
    $this->permitido = $perm;     //
    if (!$this->seguridad())       // user is not allowed
      if ($this->user->loged)      // maybe is not logged in
      {
        $this->user->log("Unauthorised access attempt.");     // if logged in but not allowed
        $this->send_response(array(
          'code'        => 401,
          'error'       => 'Unauthorized',
          'details'     => 'Acceso restringido'
        ), true);
      } else                                                    // User not logged
      {
        $this->user->log("Attempted to access with no loged user.");
        $this->send_response(array("error" => 'No user loged. Please, login.'), true);
      }
   }

  protected function getPag()       // retrieves pagination variables passed by POST method
   {
    $pag[0] = isset($_POST['page']) ? $_POST['page'] : 0;
    $pag[1] = isset($_POST['itemsPerPage']) ? $_POST['itemsPerPage'] : 0;
    return $pag;
   }
  protected function login()         // log in
   {
    if (isset($_POST['token']))                                                     //if exists, auth token stored in $token
      $token = $_POST['token'];
    else {
      $this->user->log("Failed autoLogin attempt: no token provided");            // no token provided error

      $this->send_response(array(
        'code'        => 498,
        'error'        => 'Invalid token. No token provided',
        'details'      => ''
      ), true);
    }
    if (strlen($token) != 32)                                                         // bad token
    {
      $this->user->log("Failed autoLogin attempt: invalid token $token");
      $this->send_response(array(
        'code'        => 499,
        'error'        => 'Invalid token',
        'details'      => 'Invalid token'
      ), true);
    }
    $id_user = $this->user->login();                                                // Autenticates token and retrieve userid

    if ($id_user !== true)                                                         // the user couldn't logg in
    {
      $this->user->log("Failed autologin attempt: not found $token");
      $this->send_response(array(
        'code'        => 404,
        'error'       => 'Not found',
        'details'     => $id_user
      ), true);
    }
   }
}
