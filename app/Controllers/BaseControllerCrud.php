<?php namespace App\Controllers;

  include(APPPATH . 'Libraries/GroceryCrudEnterprise/autoload.php');
  use GroceryCrud\Core\GroceryCrud;

class BaseControllerCrud extends BaseController
{

  protected function output($output = null) 
    {
      
      if (isset($output->isJSONResponse) && $output->isJSONResponse) 
      {
        header('Content-Type: application/json; charset=utf-8');
        echo $output->output;
        exit;
      }
      return view('Gc.php', (array)$output);
    }
  protected function _getDbData() 
    {
      $db = (new \Config\Database())->default;
      return [
              'adapter' => 
                  [
                  'driver' => getenv('database.default.DBDriver'),
                  'host'     => getenv('database.default.hostname'),
                  'database' => getenv('database.default.database'),
                  'username' => getenv('database.default.username'),
                  'password' => getenv('database.default.password'),
                  'charset' => 'utf8'
                  ]
             ];
     }
  protected function _getGroceryCrudEnterprise($bootstrap = true, $jquery = true) 
    {
      $db = $this->_getDbData();
      $config = (new \Config\GroceryCrudEnterprise())->getDefaultConfig();
      $groceryCrud = new GroceryCrud($config, $db);
      return $groceryCrud;
    }

}