<?php

namespace App\Controllers;

/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 *
 * @package CodeIgniter
 */
use CodeIgniter\Controller;

class BaseController extends Controller
{

 public $user;
 public $permitido;    //categorias autorizadas a ejecutar un controlador
 public $userCat;      //categorias del usuario activo
 protected $helpers = [];
 public function seguridad($permitido=array())
  {
   if (count($permitido) == 0)
    $permitido = $this->permitido;
   if (isset($_POST['token']) or !$this->user->loged) // user not loged or token passed. Login
    $this->login();
   if (in_array(256, $this->user->userLevels) or $permitido[0] == 0) // tiene permisos de Bofh o se permite a todo el mundo
    return true;
   foreach ($permitido as $p)
   {
    if (in_array($p, $this->user->userLevels)) // en cuanto encontremos que tiene uno de los permisos que autorizan,
    //  retornamos true
     return true;
   else
    return false;
   } 
  }
 public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
  {
  // Do Not Edit This Line
  parent::initController($request, $response, $logger);
  $this->user = New \App\Models\User2();
  //--------------------------------------------------------------------
  // Preload any models, libraries, etc, here.
  //--------------------------------------------------------------------
  // E.g.:
  // $this->session = \Config\Services::session();
  }
}
