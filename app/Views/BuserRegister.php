<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1">
  <title></title>
      <link rel=StyleSheet href="_style.css" type="text/css" media=screen>
</head>

<body style="margin:0;padding:0;word-spacing:normal;background-color:#fffddd;">
  <div role="article" aria-roledescription="email" lang="en" style="text-size-adjust:100%;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;background-color:#fffddd;">
    <table role="presentation" style="width:100%;border:none;border-spacing:0;">
      <tr>
        <td align="center" style="padding:0;">
          <table role="presentation" style="width:94%;max-width:600px;border:none;border-spacing:0;text-align:left;font-family:Arial,sans-serif;font-size:16px;line-height:22px;color:#363636;">
            <tr>
              <td style="padding:40px 30px 30px 30px;text-align:center;font-size:24px;font-weight:bold;">
               <a href="http://www.example.com/" style="text-decoration:none; "><img src="https://cometwise.com/img/cometwise-logo.png" width="440" alt="" style="width:90%;height:auto;border:none;text-decoration:none;color:#363636;"></a>
              </td>
            </tr>
            <tr>
              <td style="padding:30px;background-color:#ffffff;">
                
                <h1 style="margin-top:0;margin-bottom:16px;font-size:26px;line-height:32px;font-weight:bold;letter-spacing:-0.02em;">Restablecer contraseña</h1>
              
                <p style="margin:0;">Para activar el usuario, siga <a href="<?=$link?>" style="color:#e50d70;text-decoration:underline;">ESTE ENLACE.</a>
                <br><br>
                <p>Si tu cliente de correo no te lo permite, copia y pegua el siguente enlace en su navegador:</p>
                <br><br>
                <p>
                <?=$link?>
                </p>

              </td>
            </tr>
            <tr>
              <td style="padding:0;font-size:24px;line-height:28px;font-weight:bold;background-color:#f6edb1;">
                <a href="http://www.example.com/" style="text-decoration:none;"><img src="https://cometwise.com/img/world-cometwise-working.png" width="600" alt="" style="width:100%;height:auto;display:block;border:none;text-decoration:none;color:#363636;"></a>
              </td>
            </tr>
           
            <tr>
              <td style="padding:30px;text-align:center;font-size:12px;background-color:#404040;color:#cccccc;">
                <p style="margin:0;font-size:14px;line-height:20px;">&reg; 2021 <a href="https://www.suments.com/" style="text-decoration:underline;">Suments Data</a>, We speak data! <br></p>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </div>
</body>
</html>