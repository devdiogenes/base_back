<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="<?php base_url(); ?>/grocery-crud/css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="<?php base_url(); ?>/assets/css/s.css">

    <?php
    foreach ($css_files as $file) : ?>
        <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />

    <?php endforeach; ?>

    <?php foreach ($js_files as $file) : ?>

        <script src="<?php echo $file; ?>"></script>
    <?php endforeach;  ?>

</head>

<body>
    <?php echo view('bbo/menu_left.php') ?>

    <div class="body col-sm-10">
        <div class="body-content" style="background-color: #f1f1f1; margin-top:25px; margin-left:25px; border-radius: 15px; padding: 15px; margin-right: 20px;">
            <?php echo $output; ?>
        </div>
    </div>
    </div>
    </div>
    <link rel="stylesheet" href="<?php base_url(); ?>/grocery-crud/css/bootstrap/bootstrap.min.css">
    <link rel="stylesheet" href="<?php base_url(); ?>/assets/css/s.css">

    <!-- Beginning footer -->
    <?php echo view('bbo/footer.php') ?>
    <!-- End of Footer -->
</body>

</html>