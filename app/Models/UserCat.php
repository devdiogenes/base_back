<?php
 namespace App\Models;

use CodeIgniter\Model;

class UserCat extends Model
{
  protected $table      = 'USERS_CATEGORIES';
  protected $primaryKey = 'id_ruc';
 
  protected $returnType = 'object';
  protected $useSoftDeletes = false;

  protected $allowedFields = ['id_user_category','name','level'];

  protected $useTimestamps = false;
  protected $createdField  = '';
  protected $updatedField  = '';
  protected $deletedField  = '';

  protected $validationRules    = [];
  protected $validationMessages = [];
  protected $skipValidation     = true;

}