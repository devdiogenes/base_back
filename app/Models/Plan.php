<?php

namespace App\Models;

use CodeIgniter\Model;

class Plan extends Model
{
  protected $db;
  protected $table      = 'PLANS';
  protected $primaryKey = 'id_plan';
  protected $returnType = 'object';
  protected $tempReturnType = 'object';


public function __construct()
  {
    $this->db = \Config\Database::connect();
  }
public function getUserPlans($id_user)
 {
   $qry = "
   SELECT * FROM REL_USERS_PLANS
    NATURAL JOIN PLANS 
   WHERE id_user = $id_user
   " ;
   return $this->db->query($qry)->getResult();
 }
}