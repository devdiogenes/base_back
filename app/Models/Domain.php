<?php

namespace App\Models;

use CodeIgniter\Model;

class Domain extends Model
{
  protected $db;
  protected $table      = 'REL_USERS_PLANS';
  protected $primaryKey = 'id_rup';
  protected $returnType = 'object';
  protected $tempReturnType = 'object';
  protected $allowedFields = ['id_user','id_plan','domain','date_end','remaining'];
  
  public function __construct()
   {
    $this->db = \Config\Database::connect();
   }
   /**
    *  L O A D 
    *  Retrieves info of the domain and the plan purchased. False if not found.
    */
  public function load($token)
   {
     return $this->test($token);
     
   }
  private function test($token) // test if a token is valid
   {
     $qry = "
      SELECT id_rup FROM REL_USERS_PLANS WHERE md5(id_rup) = '$token'";
     $ret = $this->db->query($qry)->getRow();
     if (is_null($ret))
      return false;
     return $ret->id_rup;
   }
}