<?php

namespace App\Models;

use CodeIgniter\Model;

class User2 extends Model
{
  protected $db;
  protected $table      = 'USERS';
  protected $primaryKey = 'id_user';

  protected $returnType = 'object';
  protected $tempReturnType = 'object';
  protected $allowedFields = ['name', 'password', 'email','language','company','phone','checked','comercial_allowed'];
  protected $useTimestamps = false;
  protected $createdField  = 'date_created';

  public $id;                       // user id field.
  public $loged = false;            // boolean. True if user is loged.
  public $userLevels;               // array con los niveles de permisos del usuario activo
  public $usuarioInterno = false;   // Flag. True if Sument's user
  public $userdata;                 // object with Database fields
  public $log;                      // object userLog

  // Constructor: initial tasks. Call setvar function if session user is active
  public function __construct()
   {
    $this->db = \Config\Database::connect();
    $this->loged = !empty($_SESSION['user']);           // if true, the user is already loged
    if ($this->loged)
      $this->setvars();
    else
      $this->log = new \App\Models\Userlog(0);          // dummy userlog
   }
  
  public function activate($key)
   {
     $qry = "
     UPDATE USERS 
      SET checked = 1
     WHERE md5(concat(concat(name,email),password)) = '$key'
     ";
    $this->db->query($qry);
    $qry = "
    SELECT * 
    FROM USERS
    WHERE md5(concat(concat(name,email),password)) = '$key'
    ";
    return ($this->db->query($qry)->getRow());
   }
  // Log: cal log model for record
  public function log($text)
   {
    $this->log->log($text);
   }

  // Logout: unset session variable
  public function logout()
   {
    unset($_SESSION['user']);
   }
  // Login: Check token, recover user, set session var and call setvars func.
  public function login($token='')
   {
    if($token == '')                    // token can come as parameter (command line) or as POST var
      $token = $_POST['token'];
    $qry = "
        SELECT id_user, name, email, date_created,language,checked
         FROM USERS
        WHERE md5(concat(name,password)) = '$token'
           OR md5(concat(email,password)) = '$token'
    ";
    $usr = $this->db->query($qry)->getRow();  // database call
    if ($usr == false)                        // not found 
      return "Invalid login/password";        // return error and end
                                              // when success
    if ($usr->checked == 0)                   // user not activated
      return "User not activated";                  
    $this->id = $usr->id_user;                // set up propertie id_user
    $_SESSION['user'] = $this->id;            // set up/update session var
    $this->loged = true;                      // set up propertie loged as true
    $this->setVars();                         // set up additional properties
    return true;                              // return true
   }
  public function checkMail($mail)
   {
     $qry = "
     SELECT * FROM USERS
     WHERE email = '$mail'";
    if(count($this->db->query($qry)->getResult()) == 0)       // not found. Then, return ok (true)
     return true;
    return false;                             // return false otherwise
   }  
  public function getPasswordFromMail($email)
    {
      $user = $this->where("email = '$email'")
                   ->find();
      if(count($user) == 0)
        return false;

      return $user[0]->password;
    }
  public function setPasswordFromEncodedMail($md5email,$password)
    {
      $varlist['password'] = $password;
      $this->where("md5(email) = '$md5email'")
           ->set(['password' => $password])
           ->update();
      
    }

   ////////////////////////////// PRIVATE FUNCTIONS ///////////////////////////////////////////

  // GetLevels: called by setVars. Read and set user levels
  private function getLevels()
   {
    $qry = " 
    SELECT level 
      FROM REL_USERS_CATEGORIES
      JOIN USERS_CATEGORIES ON (REL_USERS_CATEGORIES.id_category = USERS_CATEGORIES.id_user_category)
    WHERE id_user = {$this->id}
    ";
    $result = $this->db->query($qry)->getResult();  // database call
    foreach ($result as $r)                         //for each row
    {
      $this->userLevels[] = $r->level;              // level to an array
      if ($r->level > 1)
        $this->usuarioInterno = true;               // set flag usuario interno to true 
    }
    if (!$result)                                   // no categories where set
    {
      $this->userLevels[] = [1];                    //set to external user by default
      $this->usuarioInterno = false;                // set flag to false
    }
   }
  // setVars; set up model properties
  private function setVars()
   {
    $this->id = $_SESSION['user'];                    // take id from session var. 
    $this->log = New \App\Models\Userlog($this->id);  // record login
    $this->userdata = $this->find($this->id);         // recover user data
    unset($this->userdata->password);                 // avoid sending password
    $this->getLevels();                               // set up the userLevels array
   }
} // end of class