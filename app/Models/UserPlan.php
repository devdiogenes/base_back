<?php

namespace App\Models;

use CodeIgniter\Model;

class UserPlan extends Model
{
  protected $db;
  protected $table      = 'REL_USERS_PLANS';
  protected $primaryKey = 'id_rup';
  protected $returnType = 'object';
  protected $tempReturnType = 'object';
  protected $allowedFields = ['id_user','id_plan','domain','date_end','remaining'];
  //
}