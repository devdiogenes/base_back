<?php
 namespace App\Models;

use CodeIgniter\Model;

class Land extends Model
{
  protected $table      = 'LANDING';
  protected $primaryKey = 'id_landing';
  protected $returnType = 'object';
  protected $tempReturnType = 'object';
  protected $useSoftDeletes = false;
  protected $allowedFields = ['name','email','token','company','domain','comment', 'consent_marketing'];
}
