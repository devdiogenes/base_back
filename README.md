# SUMENTS

Here is "base_back" template for startup a new app.

## Table of Contents

- [Host]("#host")
- [Coding practices](#coding)
- [env files](#env)
- [Architecture](#architecture)
- [APP Services](#APPServices)

# Host

The virtual host configuration file must be saved on `/etc/apache2/sites-available/your_site.conf`: 

        <VirtualHost *:80>
                ServerName base.ootb
                ServerAlias www.base.ootb

                DocumentRoot /var/www/html/ootb/public
                <Directory /var/www/html/ootb >
                    Options Indexes FollowSymLinks MultiViews
                    AllowOverride All
                    Require all granted
                </Directory>
                SetEnv CI_ENVIRONMENT production

                ErrorLog ${APACHE_LOG_DIR}/error.log
                CustomLog ${APACHE_LOG_DIR}/access.log combined

            RewriteEngine on
        </VirtualHost>

Then run `sudo a2ensite`.

And then you add a host on `/etc/hosts/`:
        
        127.0.0.1      base.ootb
Finally:
        sudo service apache2 restart

Browse to:
        http://base.ootb
# Coding

## Repo, branches and its usage

The code is organized into three different types of branches:
- `master` --> main branch that will hold production release.
- `dev` --> All individual features will be integrated on this branch.
- `DD-xxx` --> Feature branch corresponding to a jira ticket witht he development details. This one should be created when work starts on a new ticket. 

## Naming

To name things we follow a small convention depending on the context. 
On base_back project, there are two main "spaces":
- `BaseApi` --> All methods fron this class must start with `B` followed by a descriptive name. i.e: "Bland", "BUserActivate".
- `BaseBackOffice` --> All methods fron this class must start with `bbo` followed by a descriptive name. i.e "bbo_login"

# env

Following the CI's website [documentation about env files](https://codeigniter.com/user_guide/general/configuration.html), we used the [env official template](https://github.com/vpmistry13/codeigniter-4/blob/master/env.example) for this.


# Architecture

The goal of this project is to provide a fundamental set of methods and services to handle common tasks to all projects. To do that, we have defines two scopes:
- API --> Controller accessible from outside to use the services. In `base_back` repo, this class should be empty.
    - `BaseApi` --> `API`'s parent class. All methods necessary to all projects.
- `BackOffice` --> Project specific backOffice functionalities. It should be empty in `base_back`.
    - `BaseBackOffice` --> `BackOffice`'s parent class. It contains all basic CRUD operations for `base` DB.

# APPServices

En `Controlles/Api.php` está la definición de la clase que hay que usar para incluir un nuevo servicio en la aplicación.

Cada serivicio que se vaya a utilizar hay que incluirlo como un nuevo método de ésta.

## Base_base

1 x Codeigniter 4

- [Info](https://codeigniter.com/user_guide/intro/index.html)

