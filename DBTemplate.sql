-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 27, 2022 at 10:13 AM
-- Server version: 8.0.31-0ubuntu0.20.04.1
-- PHP Version: 7.4.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `base`
--

-- --------------------------------------------------------

--
-- Table structure for table `GLOBAL_PARMS`
--

CREATE TABLE `GLOBAL_PARMS` (
  `parm` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_spanish_ci NOT NULL,
  `value` varchar(256) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `notes` text CHARACTER SET latin1 COLLATE latin1_swedish_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_spanish_ci;

--
-- Dumping data for table `GLOBAL_PARMS`
--

INSERT INTO `GLOBAL_PARMS` (`parm`, `value`, `notes`) VALUES
('api.verbose', 'true', NULL),
('base.SMTPPass', 'Morcillica', NULL),
('base.SMTPPort', '587', NULL),
('base.SMTPHost', 'ssl0.ovh.net', NULL),
('base.SMTPUser', 'notificaciones@cometwise.com', NULL),
('base.AfterAct', 'Login', NULL),
('bland.SendMail', 'paco@moncayo.ovh', NULL),
('siteUrl', 'https://cometwise.com', 'Valor usado para enviar los enlaces de mail.'),
('base.LandSendMail', 'paco@moncayo.ovh', NULL),
('base.FromMail', 'notificaciones@cometwise.com', 'Es el correo que aparece como remitente en las notificaciones que se emiten en Base.'),
('base.SuscribeSendMail', 'paco@moncayo.ovh', 'Direccion(es) de correo a donde se manda la notificacion de un nuevo plan suscrito.'),
('base.UserActivatedSendMail', 'paco@moncayo.ovh', 'Direccion de envio cuando una cuenta de usuario ha sido activada');

-- --------------------------------------------------------

--
-- Table structure for table `LANDING`
--

CREATE TABLE `LANDING` (
  `id_landing` int NOT NULL,
  `name` varchar(128) NOT NULL,
  `email` varchar(64) NOT NULL,
  `token` varchar(32) NOT NULL,
  `company` varchar(64) NOT NULL,
  `domain` varchar(64) NOT NULL,
  `comment` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `LANGUAGES`
--

CREATE TABLE `LANGUAGES` (
  `id_language` char(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_spanish_ci NOT NULL,
  `language` varchar(16) CHARACTER SET utf8mb3 COLLATE utf8mb3_spanish_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_spanish_ci;

--
-- Dumping data for table `LANGUAGES`
--

INSERT INTO `LANGUAGES` (`id_language`, `language`) VALUES
('EN', 'English'),
('ES', 'Español');

-- --------------------------------------------------------

--
-- Table structure for table `PLANS`
--

CREATE TABLE `PLANS` (
  `id_plan` int NOT NULL,
  `name` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_spanish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_spanish_ci;

--
-- Dumping data for table `PLANS`
--

INSERT INTO `PLANS` (`id_plan`, `name`) VALUES
(1, 'Basic'),
(2, 'Profesional');

-- --------------------------------------------------------

--
-- Table structure for table `REL_USERS_CATEGORIES`
--

CREATE TABLE `REL_USERS_CATEGORIES` (
  `id_ruc` int NOT NULL,
  `id_user` int NOT NULL,
  `id_category` int NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_spanish_ci;

-- --------------------------------------------------------

--
-- Table structure for table `REL_USERS_PLANS`
--

CREATE TABLE `REL_USERS_PLANS` (
  `id_rup` int NOT NULL,
  `id_user` int NOT NULL,
  `id_plan` int NOT NULL DEFAULT '1',
  `domain` varchar(128) NOT NULL,
  `date_ini` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `date_end` timestamp NOT NULL,
  `remaining` int NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `REL_USERS_PLANS`
--

INSERT INTO `REL_USERS_PLANS` (`id_rup`, `id_user`, `id_plan`, `domain`, `date_ini`, `date_end`, `remaining`) VALUES
(1, 1, 1, 'base.com', '2021-07-05 16:38:43', '2021-09-30 18:38:12', 0),
(4, 1, 2, 'caca.com', '2021-07-13 07:16:12', '2021-12-31 00:00:00', 500);

-- --------------------------------------------------------

--
-- Table structure for table `USERS`
--

CREATE TABLE `USERS` (
  `id_user` int NOT NULL,
  `name` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_spanish_ci NOT NULL,
  `password` varchar(16) CHARACTER SET utf8mb3 COLLATE utf8mb3_spanish_ci NOT NULL,
  `email` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_spanish_ci NOT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `language` char(3) CHARACTER SET utf8mb3 COLLATE utf8mb3_spanish_ci NOT NULL DEFAULT 'ES',
  `company` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_spanish_ci DEFAULT NULL,
  `phone` varchar(16) CHARACTER SET utf8mb3 COLLATE utf8mb3_spanish_ci DEFAULT NULL,
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `comercial_allowed` char(5) COLLATE utf8mb3_spanish_ci NOT NULL DEFAULT 'false'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_spanish_ci;

--
-- Dumping data for table `USERS`
--

INSERT INTO `USERS` (`id_user`, `name`, `password`, `email`, `language`, `company`, `phone`, `checked`, `comercial_allowed`) VALUES
(1, 'pofenas', 'DummyPassword', 'paco@moncayo.ovh', 'ES', 'Pofesoft', '77766', 1, 'false'),
(24, 'Yael', 'DummyPassword', 'yael@suments.com', 'ES', 'Suments Data', NULL, 1, 'false'),
(23, 'Javi', 'DummyPassword', 'javi@suments.com', 'ES', 'Suments Data', NULL, 1, 'false'),
(22, 'Jose Ramon', 'DummyPassword', 'joseramon@suments.com', 'ES', 'Suments', NULL, 1, 'false'),
(21, 'Alonso', 'DummyPassword', 'alonso@suments.com', 'ES', NULL, NULL, 1, 'false'),
(19, 'automated_test', 'DummyPassword', 'autotest@suments.com', 'ES', '', '', 1, 'false'),
(20, 'Irma', 'DummyPassword', 'irma@suments.com', 'ES', 'Suments Data', NULL, 1, 'false'),
(25, 'Irma', 'DummyPassword', 'hola@irma', 'ES', '', '', 0, 'false'),
(26, '', 'DummyPassword', '', 'ES', '', '', 0, 'false'),
(27, 'ñaskdf', 'DummyPassword', 'asdf', 'ES', '', '', 0, 'false'),
(28, 'hola@irma', 'DummyPassword', 'hola@irma.com', 'ES', '', '', 0, 'false'),
(29, 'Irma', 'DummyPassword', 'irma', 'ES', '', '', 0, 'false'),
(30, '', 'DummyPassword', 'hola@suments.com', 'ES', '', '', 0, 'false'),
(31, 'Irma', 'DummyPassword', 'yo@suments.com', 'ES', '', '', 0, 'false'),
(32, 'irma', 'DummyPassword', 'irmaa@suments.com', 'ES', '', '', 0, 'false'),
(33, 'Diego', 'DummyPassword', 'diego@suments.com', 'ES', 'Suments Data', NULL, 1, 'false');

-- --------------------------------------------------------

--
-- Table structure for table `USERS_CATEGORIES`
--

CREATE TABLE `USERS_CATEGORIES` (
  `id_user_category` int NOT NULL,
  `name` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_spanish_ci NOT NULL,
  `level` int NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_spanish_ci;

--
-- Dumping data for table `USERS_CATEGORIES`
--

INSERT INTO `USERS_CATEGORIES` (`id_user_category`, `name`, `level`) VALUES
(1, 'Usuario externo', 1),
(2, 'Administrativo', 2),
(3, 'Comercial', 4),
(4, 'Desarrollador', 8),
(5, 'Dirección', 16),
(6, 'Bofh', 256);

-- --------------------------------------------------------

--
-- Table structure for table `USER_LOG`
--

CREATE TABLE `USER_LOG` (
  `id_user_log` int NOT NULL,
  `action` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_spanish_ci DEFAULT '-',
  `date_action` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `id_user` int NOT NULL,
  `ip_user` varchar(16) CHARACTER SET utf8mb3 COLLATE utf8mb3_spanish_ci NOT NULL DEFAULT 'UNKNOWN'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_spanish_ci;

--
-- Dumping data for table `USER_LOG`
--

INSERT INTO `USER_LOG` (`id_user_log`, `action`, `date_action`, `id_user`, `ip_user`) VALUES
(12, 'pofenas: plan list obtained.', '2021-07-14 08:23:27', 1, '127.0.0.1'),
(13, 'pofenas: plan list obtained.', '2021-07-14 08:24:23', 1, '127.0.0.1'),
(14, 'Loged user pofenas', '2021-07-14 09:06:04', 1, '93.96.107.199'),
(15, 'Failed autoLogin attempt: no token provided', '2021-07-14 09:10:48', 0, '93.96.107.199'),
(16, 'Failed autoLogin attempt: no token provided', '2021-07-14 09:11:53', 0, '93.96.107.199'),
(17, 'Failed autoLogin attempt: no token provided', '2021-07-14 09:13:16', 0, '93.96.107.199'),
(18, 'Failed autoLogin attempt: no token provided', '2021-07-14 09:13:20', 0, '93.96.107.199'),
(19, 'Failed autoLogin attempt: no token provided', '2021-07-14 09:13:55', 0, '93.96.107.199'),
(20, 'Failed autoLogin attempt: no token provided', '2021-07-14 09:15:41', 0, '93.96.107.199'),
(21, 'Loged user pofenas', '2021-07-14 11:05:12', 1, '93.96.107.199'),
(22, 'Failed autologin attempt: not found bc4f1ba327a1e2b3ff206f5d69bdc53a', '2021-07-14 11:09:13', 0, '93.96.107.199'),
(23, 'Failed autologin attempt: not found bc4f1ba327a1e2b3ff206f5d69bdc53a', '2021-07-14 11:10:51', 0, '93.96.107.199'),
(24, 'Failed autologin attempt: not found bc4f1ba327a1e2b3ff206f5d69bdc53a', '2021-07-14 11:10:56', 0, '93.96.107.199'),
(25, 'Failed autologin attempt: not found bc4f1ba327a1e2b3ff206f5d69bdc53a', '2021-07-14 11:10:58', 0, '93.96.107.199'),
(26, 'Failed autologin attempt: not found bc4f1ba327a1e2b3ff206f5d69bdc53a', '2021-07-14 11:10:59', 0, '93.96.107.199'),
(27, 'Failed Login attempt: not found bc4f1ba327a1e2b3ff206f5d69bdc53a', '2021-07-14 11:11:30', 0, '93.96.107.199'),
(28, 'Failed Login attempt: not found bc4f1ba327a1e2b3ff206f5d69bdc53a', '2021-07-14 11:11:52', 0, '93.96.107.199'),
(29, 'Loged user pofenas', '2021-07-14 11:12:14', 1, '93.96.107.199'),
(30, 'Loged user pofenas', '2021-07-14 11:18:33', 1, '93.96.107.199'),
(31, 'Loged user pofenas', '2021-07-14 11:24:56', 1, '93.96.107.199'),
(32, 'Loged user pofenas', '2021-07-14 11:27:45', 1, '93.96.107.199'),
(33, 'Loged user pofenas', '2021-07-14 11:28:01', 1, '93.96.107.199'),
(34, 'Failed Login attempt: not found 4c35b5186c5fbd8d1719f0761d4cecd4', '2021-07-14 11:28:33', 0, '93.96.107.199'),
(35, 'Failed Login attempt: not found 4c35b5186c5fbd8d1719f0761d4cecd4', '2021-07-14 11:28:37', 0, '93.96.107.199'),
(36, 'Failed Login attempt: not found 4c35b5186c5fbd8d1719f0761d4cecd4', '2021-07-14 11:35:18', 0, '93.96.107.199'),
(37, 'Failed Login attempt: not found 4c35b5186c5fbd8d1719f0761d4cecd4', '2021-07-14 11:35:22', 0, '93.96.107.199'),
(38, 'Failed Login attempt: not found 4c35b5186c5fbd8d1719f0761d4cecd4', '2021-07-14 11:36:12', 0, '93.96.107.199'),
(39, 'Loged user pofenas', '2021-07-14 11:36:22', 1, '93.96.107.199'),
(40, 'Loged user pofenas', '2021-07-14 11:41:32', 1, '93.96.107.199'),
(41, 'Loged user pofenas', '2021-07-14 11:41:56', 1, '93.96.107.199'),
(42, 'Loged user pofenas', '2021-07-14 11:42:31', 1, '93.96.107.199'),
(43, 'Loged user pofenas', '2021-07-14 11:51:28', 1, '93.96.107.199'),
(44, 'Loged user pofenas', '2021-07-14 11:54:52', 1, '93.96.107.199'),
(45, 'Loged user pofenas', '2021-07-14 11:56:51', 1, '93.96.107.199'),
(46, 'Loged user pofenas', '2021-07-14 11:58:33', 1, '93.96.107.199'),
(47, 'Loged user pofenas', '2021-07-14 11:59:07', 1, '93.96.107.199'),
(48, 'Loged user pofenas', '2021-07-14 12:00:21', 1, '93.96.107.199'),
(49, 'Loged user pofenas', '2021-07-14 12:01:36', 1, '93.96.107.199'),
(50, 'Logout user <not loged>', '2021-07-14 12:01:36', 0, '93.96.107.199'),
(51, 'Logout user <not loged>', '2021-07-14 12:02:43', 0, '93.96.107.199'),
(52, 'Loged user pofenas', '2021-07-14 12:02:57', 1, '93.96.107.199'),
(53, 'Loged user pofenas', '2021-07-14 12:03:11', 1, '93.96.107.199'),
(54, 'Loged user pofenas', '2021-07-14 12:04:19', 1, '93.96.107.199'),
(55, 'Logout user <not loged>', '2021-07-14 12:04:19', 0, '93.96.107.199'),
(56, 'Logout user <not loged>', '2021-07-14 12:04:51', 0, '93.96.107.199'),
(57, 'Failed Login attempt: not found c65c1708d410795fb15426724febf046', '2021-07-14 12:04:58', 0, '93.96.107.199'),
(58, 'Loged user pofenas', '2021-07-14 12:05:08', 1, '93.96.107.199'),
(59, 'Logout user <not loged>', '2021-07-14 12:05:09', 0, '93.96.107.199'),
(60, 'Loged user pofenas', '2021-07-14 12:05:24', 1, '93.96.107.199'),
(61, 'Logout user <not loged>', '2021-07-14 12:05:24', 0, '93.96.107.199'),
(62, 'Loged user pofenas', '2021-07-14 12:06:25', 1, '93.96.107.199'),
(63, 'Logout user <not loged>', '2021-07-14 12:06:25', 0, '93.96.107.199'),
(64, 'Loged user pofenas', '2021-07-14 12:09:17', 1, '93.96.107.199'),
(65, 'Logout user <not loged>', '2021-07-14 12:09:28', 0, '93.96.107.199'),
(66, 'Loged user pofenas', '2021-07-14 12:09:36', 1, '93.96.107.199'),
(67, 'Logout user <not loged>', '2021-07-14 12:09:36', 0, '93.96.107.199'),
(68, 'Loged user pofenas', '2021-07-14 12:09:57', 1, '93.96.107.199'),
(69, 'Loged user pofenas', '2021-07-14 12:10:02', 1, '93.96.107.199'),
(70, 'Loged user pofenas', '2021-07-14 12:10:03', 1, '93.96.107.199'),
(71, 'Loged user pofenas', '2021-07-14 12:10:03', 1, '93.96.107.199'),
(72, 'Loged user pofenas', '2021-07-14 12:10:04', 1, '93.96.107.199'),
(73, 'Loged user pofenas', '2021-07-14 12:10:04', 1, '93.96.107.199'),
(74, 'Logout user <not loged>', '2021-07-14 12:11:14', 0, '93.96.107.199'),
(75, 'Loged user pofenas', '2021-07-14 12:11:27', 1, '93.96.107.199'),
(76, 'Logout user <not loged>', '2021-07-14 12:11:27', 0, '93.96.107.199'),
(77, 'Loged user pofenas', '2021-07-14 12:12:06', 1, '93.96.107.199'),
(78, 'Logout user <not loged>', '2021-07-14 12:12:31', 0, '93.96.107.199'),
(79, 'Loged user pofenas', '2021-07-14 12:12:36', 1, '93.96.107.199'),
(80, 'Logout user <not loged>', '2021-07-14 12:12:37', 0, '93.96.107.199'),
(81, 'Loged user pofenas', '2021-07-14 12:13:41', 1, '93.96.107.199'),
(82, 'Logout user <not loged>', '2021-07-14 12:13:41', 0, '93.96.107.199'),
(83, 'Loged user pofenas', '2021-07-14 12:14:06', 1, '93.96.107.199'),
(84, 'Loged user pofenas', '2021-07-14 12:15:10', 1, '93.96.107.199'),
(85, 'Loged user pofenas', '2021-07-14 12:16:11', 1, '93.96.107.199'),
(86, 'Loged user pofenas', '2021-07-14 12:16:39', 1, '93.96.107.199'),
(87, 'Loged user pofenas', '2021-07-14 12:17:23', 1, '93.96.107.199'),
(88, 'Loged user pofenas', '2021-07-14 12:19:16', 1, '93.96.107.199'),
(89, 'Logout user <not loged>', '2021-07-14 12:19:16', 0, '93.96.107.199'),
(90, 'Loged user pofenas', '2021-07-14 12:20:30', 1, '93.96.107.199'),
(91, 'Logout user <not loged>', '2021-07-14 12:20:30', 0, '93.96.107.199'),
(92, 'Failed Login attempt: not found eb6ebe6105989f5f2ab85c092b5aab27', '2021-07-14 12:21:31', 0, '93.96.107.199'),
(93, 'Loged user pofenas', '2021-07-14 12:21:42', 1, '93.96.107.199'),
(94, 'Logout user <not loged>', '2021-07-14 12:21:42', 0, '93.96.107.199'),
(95, 'Loged user pofenas', '2021-07-14 12:22:49', 1, '93.96.107.199'),
(96, 'Logout user <not loged>', '2021-07-14 12:22:49', 0, '93.96.107.199'),
(97, 'Loged user pofenas', '2021-07-14 12:23:36', 1, '93.96.107.199'),
(98, 'Logout user <not loged>', '2021-07-14 12:23:36', 0, '93.96.107.199'),
(99, 'Loged user pofenas', '2021-07-14 12:24:07', 1, '93.96.107.199'),
(100, 'Logout user <not loged>', '2021-07-14 12:24:07', 0, '93.96.107.199'),
(101, 'Loged user pofenas', '2021-07-14 12:24:29', 1, '93.96.107.199'),
(102, 'Logout user <not loged>', '2021-07-14 12:27:48', 0, '93.96.107.199'),
(103, 'Loged user pofenas', '2021-07-14 12:27:57', 1, '93.96.107.199'),
(104, 'Logout user <not loged>', '2021-07-14 12:27:58', 0, '93.96.107.199'),
(105, 'Loged user pofenas', '2021-07-14 12:28:25', 1, '93.96.107.199'),
(106, 'Loged user pofenas', '2021-07-14 12:29:10', 1, '93.96.107.199'),
(107, 'Loged user pofenas', '2021-07-14 12:31:46', 1, '93.96.107.199'),
(108, 'Loged user pofenas', '2021-07-14 12:33:31', 1, '93.96.107.199'),
(109, 'Failed autologin attempt: not found bc4f1ba327a1e2b3ff206f5d69bdc53a', '2021-07-14 12:33:43', 0, '93.96.107.199'),
(110, 'Failed Login attempt: not found bc4f1ba327a1e2b3ff206f5d69bdc53a', '2021-07-14 12:34:15', 0, '93.96.107.199'),
(111, 'Failed Login attempt: not found bc4f1ba327a1e2b3ff206f5d69bdc53a', '2021-07-14 12:34:40', 0, '93.96.107.199'),
(112, 'Failed Login attempt: not found bc4f1ba327a1e2b3ff206f5d69bdc53a', '2021-07-14 12:34:43', 0, '93.96.107.199'),
(113, 'Loged user pofenas', '2021-07-14 12:36:37', 1, '93.96.107.199'),
(114, 'Failed autologin attempt: not found bc4f1ba327a1e2b3ff206f5d69bdc53a', '2021-07-14 12:36:46', 0, '93.96.107.199'),
(115, 'Failed autologin attempt: not found bc4f1ba327a1e2b3ff206f5d69bdc53a', '2021-07-14 12:36:47', 0, '93.96.107.199'),
(116, 'Failed autologin attempt: not found bc4f1ba327a1e2b3ff206f5d69bdc53a', '2021-07-14 12:36:48', 0, '93.96.107.199'),
(117, 'Failed autologin attempt: not found bc4f1ba327a1e2b3ff206f5d69bdc53a', '2021-07-14 12:36:49', 0, '93.96.107.199'),
(118, 'Failed autologin attempt: not found bc4f1ba327a1e2b3ff206f5d69bdc53a', '2021-07-14 12:36:50', 0, '93.96.107.199'),
(119, 'Failed autologin attempt: not found bc4f1ba327a1e2b3ff206f5d69bdc53a', '2021-07-14 12:37:02', 0, '93.96.107.199'),
(120, 'Failed autologin attempt: not found bc4f1ba327a1e2b3ff206f5d69bdc53a', '2021-07-14 12:37:02', 0, '93.96.107.199'),
(121, 'Failed autoLogin attempt: no token provided', '2021-07-14 12:37:09', 0, '93.96.107.199'),
(122, 'Failed autoLogin attempt: no token provided', '2021-07-14 12:37:15', 0, '93.96.107.199'),
(123, 'Failed Login attempt: not found eb6ebe6105989f5f2ab85c092b5aab27', '2021-07-14 12:37:20', 0, '93.96.107.199'),
(124, 'Failed Login attempt: not found bc4f1ba327a1e2b3ff206f5d69bdc53a', '2021-07-14 12:37:24', 0, '93.96.107.199'),
(125, 'Loged user pofenas', '2021-07-14 12:38:23', 1, '93.96.107.199'),
(126, 'Loged user pofenas', '2021-07-15 09:18:20', 1, '93.96.107.199'),
(127, 'Loged user pofenas', '2021-07-15 09:19:43', 1, '93.96.107.199'),
(128, 'Logout user <not loged>', '2021-07-15 09:19:43', 0, '93.96.107.199'),
(129, 'Loged user pofenas', '2021-07-15 09:20:06', 1, '93.96.107.199'),
(130, 'Logout user <not loged>', '2021-07-15 09:20:31', 0, '93.96.107.199'),
(131, 'Loged user pofenas', '2021-07-15 09:21:42', 1, '93.96.107.199'),
(132, 'Logout user <not loged>', '2021-07-15 09:21:42', 0, '93.96.107.199'),
(133, 'Loged user pofenas', '2021-07-15 09:24:08', 1, '93.96.107.199'),
(134, 'Loged user pofenas', '2021-07-15 09:34:44', 1, '93.96.107.199'),
(135, 'Loged user pofenas', '2021-07-15 09:36:27', 1, '93.96.107.199'),
(136, 'Loged user pofenas', '2021-07-15 19:25:11', 1, '93.96.107.199'),
(137, 'Failed autoLogin attempt: no token provided', '2021-07-16 07:49:52', 0, '84.126.188.251'),
(138, 'Loged user pofenas', '2021-07-16 07:50:05', 1, '84.126.188.251'),
(139, 'Failed autoLogin attempt: no token provided', '2021-07-16 07:55:28', 0, '84.126.188.251'),
(140, 'Failed autoLogin attempt: no token provided', '2021-07-16 07:55:31', 0, '84.126.188.251'),
(141, 'Failed autoLogin attempt: no token provided', '2021-07-16 07:56:37', 0, '84.126.188.251'),
(142, 'Failed autoLogin attempt: no token provided', '2021-07-16 07:56:58', 0, '84.126.188.251'),
(143, 'Loged user pofenas', '2021-07-16 10:17:07', 1, '93.96.107.199'),
(144, 'Loged user pofenas', '2021-07-16 11:13:56', 1, '93.96.107.199'),
(145, 'Loged user pofenas', '2021-07-16 11:22:54', 1, '93.96.107.199'),
(146, 'Loged user pofenas', '2021-07-16 11:59:10', 1, '93.96.107.199'),
(147, 'Loged user pofenas', '2021-07-16 11:59:11', 1, '93.96.107.199'),
(148, 'Loged user pofenas', '2021-07-16 11:59:17', 1, '93.96.107.199'),
(149, 'Loged user pofenas', '2021-07-16 12:00:53', 1, '93.96.107.199'),
(150, 'Loged user pofenas', '2021-07-16 12:01:49', 1, '93.96.107.199'),
(151, 'Loged user pofenas', '2021-07-16 12:04:00', 1, '93.96.107.199'),
(152, 'Loged user pofenas', '2021-07-16 12:04:57', 1, '93.96.107.199'),
(153, 'Loged user pofenas', '2021-07-16 12:06:37', 1, '93.96.107.199'),
(154, 'Failed Login attempt: not found 9051f3fb5114236ea42d1628d0a1888c', '2021-07-19 14:02:31', 0, '188.43.136.32'),
(155, 'demo: new user registration.', '2021-07-19 14:03:08', 0, '188.43.136.32'),
(156, 'Failed Login attempt: not found 9051f3fb5114236ea42d1628d0a1888c', '2021-07-19 14:03:21', 0, '188.43.136.32'),
(157, 'Failed Login attempt: no token provided', '2021-07-19 14:14:25', 0, '188.43.136.32'),
(158, 'Failed Login attempt: not found 9051f3fb5114236ea42d1628d0a1888c', '2021-07-19 14:16:08', 0, '188.43.136.32'),
(159, 'Failed Login attempt: not found ef22720242384aa74a67729654504650', '2021-07-19 14:17:12', 0, '188.43.136.32'),
(160, 'Failed Login attempt: not found 338ac53194834a2cdad34a2217cfeb45', '2021-07-19 14:29:41', 0, '188.43.136.32'),
(161, 'Failed Login attempt: not found 93b885adfe0da089cdf634904fd59f71', '2021-07-19 14:29:53', 0, '188.43.136.32'),
(162, 'Failed Login attempt: not found 93b885adfe0da089cdf634904fd59f71', '2021-07-19 14:30:44', 0, '188.43.136.32'),
(163, 'Failed Login attempt: not found 93b885adfe0da089cdf634904fd59f71', '2021-07-19 14:40:08', 0, '188.43.136.32'),
(164, 'Loged user pofenas', '2021-07-19 22:02:39', 1, '93.96.107.199'),
(165, 'Loged user pofenas', '2021-07-19 22:11:11', 1, '188.43.136.32'),
(166, 'Loged user pofenas', '2021-07-19 22:11:34', 1, '188.43.136.32'),
(167, 'Loged user pofenas', '2021-07-19 22:24:07', 1, '188.43.136.32'),
(168, 'Loged user pofenas', '2021-07-20 00:05:53', 1, '188.43.136.32'),
(169, 'Loged user pofenas', '2021-07-20 00:14:35', 1, '188.43.136.32'),
(170, 'Loged user pofenas', '2021-07-20 00:33:53', 1, '188.43.136.32'),
(171, 'Loged user pofenas', '2021-07-20 00:44:43', 1, '188.43.136.32'),
(172, 'Loged user pofenas', '2021-07-20 00:50:17', 1, '188.43.136.32'),
(173, 'Loged user pofenas', '2021-07-20 00:59:08', 1, '188.43.136.32'),
(174, 'Loged user pofenas', '2021-07-20 01:06:17', 1, '188.43.136.32'),
(175, 'Loged user pofenas', '2021-07-20 01:22:37', 1, '188.43.136.32'),
(176, 'Loged user pofenas', '2021-07-20 01:39:27', 1, '188.43.136.32'),
(177, 'Loged user pofenas', '2021-07-20 01:58:10', 1, '188.43.136.32'),
(178, 'Loged user pofenas', '2021-07-20 02:22:06', 1, '188.43.136.32'),
(179, 'Loged user pofenas', '2021-07-20 02:22:06', 1, '188.43.136.32'),
(180, 'Loged user pofenas', '2021-07-20 02:22:06', 1, '188.43.136.32'),
(181, 'Loged user pofenas', '2021-07-20 02:22:06', 1, '188.43.136.32'),
(182, 'Loged user pofenas', '2021-07-20 02:36:12', 1, '188.43.136.32'),
(183, 'Loged user pofenas', '2021-07-20 02:37:03', 1, '188.43.136.32'),
(184, 'Loged user pofenas', '2021-07-20 02:37:41', 1, '188.43.136.32'),
(185, 'Loged user pofenas', '2021-07-20 02:37:41', 1, '188.43.136.32'),
(186, 'Loged user pofenas', '2021-07-20 03:44:24', 1, '188.43.136.32'),
(187, 'Loged user pofenas', '2021-07-20 03:45:06', 1, '188.43.136.32'),
(188, 'Loged user pofenas', '2021-07-20 03:51:10', 1, '188.43.136.32'),
(189, 'Loged user pofenas', '2021-07-20 03:51:50', 1, '188.43.136.32'),
(190, 'Loged user pofenas', '2021-07-20 03:52:59', 1, '188.43.136.32'),
(191, 'Loged user pofenas', '2021-07-20 04:08:33', 1, '188.43.136.32'),
(192, 'Loged user pofenas', '2021-07-20 04:10:01', 1, '188.43.136.32'),
(193, 'Loged user pofenas', '2021-07-20 04:10:36', 1, '188.43.136.32'),
(194, 'Loged user pofenas', '2021-07-20 04:12:11', 1, '188.43.136.32'),
(195, 'Loged user pofenas', '2021-07-20 04:12:28', 1, '188.43.136.32'),
(196, 'Loged user pofenas', '2021-07-20 04:18:06', 1, '188.43.136.32'),
(197, 'Loged user pofenas', '2021-07-20 06:29:38', 1, '188.43.136.32'),
(198, 'Loged user pofenas', '2021-07-20 06:30:34', 1, '188.43.136.32'),
(199, 'Loged user pofenas', '2021-07-20 06:31:03', 1, '188.43.136.32'),
(200, 'Loged user pofenas', '2021-07-20 06:36:53', 1, '188.43.136.32'),
(201, 'Loged user pofenas', '2021-07-20 06:39:14', 1, '188.43.136.32'),
(202, 'Loged user pofenas', '2021-07-20 06:42:14', 1, '188.43.136.32'),
(203, 'Loged user pofenas', '2021-07-20 06:42:57', 1, '188.43.136.32'),
(204, 'Loged user pofenas', '2021-07-20 06:47:16', 1, '188.43.136.32'),
(205, 'Loged user pofenas', '2021-07-20 06:49:09', 1, '188.43.136.32'),
(206, 'Loged user pofenas', '2021-07-20 06:55:46', 1, '188.43.136.32'),
(207, 'Loged user pofenas', '2021-07-20 06:57:46', 1, '188.43.136.32'),
(208, 'Loged user pofenas', '2021-07-20 07:14:49', 1, '188.43.136.32'),
(209, 'Loged user pofenas', '2021-07-20 07:38:28', 1, '188.43.136.32'),
(210, 'Loged user pofenas', '2021-07-20 07:40:32', 1, '188.43.136.32'),
(211, 'Loged user pofenas', '2021-07-20 07:41:11', 1, '188.43.136.32'),
(212, 'Loged user pofenas', '2021-07-20 08:01:52', 1, '188.43.136.32'),
(213, 'Loged user pofenas', '2021-07-20 08:14:07', 1, '188.43.136.32'),
(214, 'Loged user pofenas', '2021-07-20 08:40:21', 1, '188.43.136.32'),
(215, 'Loged user pofenas', '2021-07-20 08:52:57', 1, '188.43.136.32'),
(216, 'Loged user pofenas', '2021-07-20 09:13:38', 1, '188.43.136.32'),
(217, 'Loged user pofenas', '2021-07-20 09:13:44', 1, '188.43.136.32'),
(218, 'Loged user pofenas', '2021-07-20 09:46:48', 1, '188.43.136.32'),
(219, 'Loged user pofenas', '2021-07-20 12:11:32', 1, '93.96.107.199'),
(220, 'Loged user pofenas', '2021-07-20 12:11:59', 1, '93.96.107.199'),
(221, 'Loged user pofenas', '2021-07-20 12:39:25', 1, '188.43.136.32'),
(222, 'Loged user pofenas', '2021-07-20 12:39:58', 1, '188.43.136.32'),
(223, 'Loged user pofenas', '2021-07-20 12:47:24', 1, '188.43.136.32'),
(224, 'Loged user pofenas', '2021-07-20 12:47:57', 1, '188.43.136.32'),
(225, 'Loged user pofenas', '2021-07-20 13:00:22', 1, '188.43.136.32'),
(226, 'Loged user pofenas', '2021-07-20 13:00:41', 1, '188.43.136.32'),
(227, 'Loged user pofenas', '2021-07-20 13:01:01', 1, '188.43.136.32'),
(228, 'Loged user pofenas', '2021-07-20 13:08:51', 1, '188.43.136.32'),
(229, 'Loged user pofenas', '2021-07-20 13:11:04', 1, '188.43.136.32'),
(230, 'Loged user pofenas', '2021-07-20 13:11:34', 1, '188.43.136.32'),
(231, 'Loged user pofenas', '2021-07-20 13:12:00', 1, '188.43.136.32'),
(232, 'Loged user pofenas', '2021-07-20 13:12:20', 1, '188.43.136.32'),
(233, 'Loged user pofenas', '2021-07-20 13:12:48', 1, '188.43.136.32'),
(234, 'Loged user pofenas', '2021-07-20 13:23:27', 1, '188.43.136.32'),
(235, 'Loged user pofenas', '2021-07-20 13:45:13', 1, '188.43.136.32'),
(236, 'Loged user pofenas', '2021-07-20 14:07:15', 1, '188.43.136.32'),
(237, 'Loged user pofenas', '2021-07-20 14:07:20', 1, '188.43.136.32'),
(238, 'Loged user pofenas', '2021-07-20 14:07:23', 1, '188.43.136.32'),
(239, 'Loged user pofenas', '2021-07-20 14:07:38', 1, '188.43.136.32'),
(240, 'Loged user pofenas', '2021-07-20 14:09:27', 1, '188.43.136.32'),
(241, 'Loged user pofenas', '2021-07-20 14:09:33', 1, '188.43.136.32'),
(242, 'Loged user pofenas', '2021-07-20 14:21:51', 1, '188.43.136.32'),
(243, 'Loged user pofenas', '2021-07-20 14:21:52', 1, '188.43.136.32'),
(244, 'Loged user pofenas', '2021-07-20 14:44:24', 1, '188.43.136.32'),
(245, 'Loged user pofenas', '2021-07-20 14:45:48', 1, '188.43.136.32'),
(246, 'Loged user pofenas', '2021-07-20 15:28:23', 1, '188.43.136.32'),
(247, 'Loged user pofenas', '2021-07-20 15:47:45', 1, '188.43.136.32'),
(248, 'Loged user pofenas', '2021-07-20 15:49:04', 1, '188.43.136.32'),
(249, 'Loged user pofenas', '2021-07-20 15:59:35', 1, '188.43.136.32'),
(250, 'Loged user pofenas', '2021-07-20 16:12:37', 1, '188.43.136.32'),
(251, 'Loged user pofenas', '2021-07-20 17:22:04', 1, '188.43.136.32'),
(252, 'Loged user pofenas', '2021-07-20 17:24:26', 1, '188.43.136.32'),
(253, 'Loged user pofenas', '2021-07-21 00:31:37', 1, '188.43.136.32'),
(254, 'Loged user pofenas', '2021-07-21 00:38:35', 1, '188.43.136.32'),
(255, 'Loged user pofenas', '2021-07-21 00:59:30', 1, '188.43.136.32'),
(256, 'Loged user pofenas', '2021-07-21 01:37:35', 1, '188.43.136.32'),
(257, 'Loged user pofenas', '2021-07-21 02:00:34', 1, '188.43.136.32'),
(258, 'Loged user pofenas', '2021-07-21 02:51:35', 1, '188.43.136.32'),
(259, 'Loged user pofenas', '2021-07-21 09:24:18', 1, '188.43.136.32'),
(260, 'Loged user pofenas', '2021-07-21 09:55:42', 1, '188.43.136.32'),
(261, 'Loged user pofenas', '2021-07-21 11:58:07', 1, '188.43.136.32'),
(262, 'Loged user pofenas', '2021-07-21 12:28:02', 1, '188.43.136.32'),
(263, 'Loged user pofenas', '2021-07-21 12:54:42', 1, '188.43.136.32'),
(264, 'Loged user pofenas', '2021-07-21 13:16:51', 1, '188.43.136.32'),
(265, 'Loged user pofenas', '2021-07-21 13:16:56', 1, '188.43.136.32'),
(266, 'Loged user pofenas', '2021-07-21 13:22:20', 1, '188.43.136.32'),
(267, 'Loged user pofenas', '2021-07-21 13:22:23', 1, '188.43.136.32'),
(268, 'Loged user pofenas', '2021-07-21 13:34:02', 1, '188.43.136.32'),
(269, 'Loged user pofenas', '2021-07-21 13:40:11', 1, '188.43.136.32'),
(270, 'Loged user pofenas', '2021-07-21 14:08:41', 1, '188.43.136.32'),
(271, 'Loged user pofenas', '2021-07-21 14:27:20', 1, '188.43.136.32'),
(272, 'Loged user pofenas', '2021-07-21 14:33:45', 1, '188.43.136.32'),
(273, 'Loged user pofenas', '2021-07-21 14:43:28', 1, '188.43.136.32'),
(274, 'Loged user pofenas', '2021-07-21 15:53:12', 1, '188.43.136.32'),
(275, 'Loged user pofenas', '2021-07-22 09:27:03', 1, '188.43.136.32'),
(276, 'Loged user pofenas', '2021-07-22 09:28:00', 1, '188.43.136.32'),
(277, 'Loged user pofenas', '2021-07-22 11:11:10', 1, '93.96.107.199'),
(278, 'Failed Login attempt: not found 93b885adfe0da089cdf634904fd59f71', '2021-07-22 11:23:25', 0, '188.43.136.32'),
(279, 'Loged user pofenas', '2021-07-22 11:41:43', 1, '93.96.107.199'),
(280, 'Loged user pofenas', '2021-07-22 11:50:02', 1, '93.96.107.199'),
(281, 'Loged user pofenas', '2021-07-22 11:50:23', 1, '93.96.107.199'),
(282, 'Failed Login attempt: not found eb6ebe6105989f5f2ab85c092b5aab27', '2021-07-22 11:56:57', 0, '93.96.107.199'),
(283, 'Failed Login attempt: not found eb6ebe6105989f5f2ab85c092b5aab27', '2021-07-22 11:57:15', 0, '93.96.107.199'),
(284, 'Failed Login attempt: not found eb6ebe6105989f5f2ab85c092b5aab27', '2021-07-22 11:57:32', 0, '93.96.107.199'),
(285, 'Failed Login attempt: not found eb6ebe6105989f5f2ab85c092b5aab27', '2021-07-22 11:58:08', 0, '93.96.107.199'),
(286, 'Failed Login attempt: not found eb6ebe6105989f5f2ab85c092b5aab27', '2021-07-22 11:58:13', 0, '93.96.107.199'),
(287, 'Failed Login attempt: not found eb6ebe6105989f5f2ab85c092b5aab27', '2021-07-22 11:59:00', 0, '93.96.107.199'),
(288, 'Failed Login attempt: not found eb6ebe6105989f5f2ab85c092b5aab27', '2021-07-22 12:00:07', 0, '93.96.107.199'),
(289, 'Failed autoLogin attempt: no token provided', '2021-07-22 12:00:41', 0, '93.96.107.199'),
(290, 'Failed Login attempt: not found 93b885adfe0da089cdf634904fd59f71', '2021-07-22 16:44:32', 0, '84.126.188.251'),
(291, 'Failed autoLogin attempt: no token provided', '2021-07-22 16:47:10', 0, '84.126.188.251'),
(292, 'Failed autoLogin attempt: no token provided', '2021-07-22 16:47:18', 0, '84.126.188.251'),
(293, 'Failed autoLogin attempt: no token provided', '2021-07-22 16:47:22', 0, '84.126.188.251'),
(294, 'Loged user pofenas', '2021-07-22 16:47:52', 1, '84.126.188.251'),
(295, 'Failed Login attempt: not found eb6ebe6105989f5f2ab85c092b5aab27', '2021-07-22 18:52:55', 0, '93.96.107.199'),
(296, 'Loged user pofenas', '2021-07-22 18:53:00', 1, '93.96.107.199'),
(297, 'Failed Login attempt: not found eb6ebe6105989f5f2ab85c092b5aab27', '2021-07-22 18:53:40', 0, '93.96.107.199'),
(298, 'Failed Login attempt: not found eb6ebe6105989f5f2ab85c092b5aab27', '2021-07-22 18:53:52', 0, '93.96.107.199'),
(299, 'Failed Login attempt: not found eb6ebe6105989f5f2ab85c092b5aab27', '2021-07-22 18:54:41', 0, '93.96.107.199'),
(300, 'Loged user pofenas', '2021-07-22 19:00:15', 1, '93.96.107.199'),
(301, 'Loged user pofenas', '2021-07-22 19:01:20', 1, '93.96.107.199'),
(302, 'Failed Login attempt: not found 93b885adfe0da089cdf634904fd59f71', '2021-07-23 08:39:31', 0, '84.126.188.251'),
(303, 'Failed Login attempt: not found 93b885adfe0da089cdf634904fd59f71', '2021-07-23 09:12:31', 0, '84.126.188.251'),
(304, 'Failed autoLogin attempt: no token provided', '2021-07-23 09:12:42', 0, '84.126.188.251'),
(305, 'Failed Login attempt: not found 93b885adfe0da089cdf634904fd59f71', '2021-07-23 09:20:05', 0, '84.126.188.251'),
(306, 'Failed Login attempt: no token provided', '2021-07-23 09:21:41', 0, '84.126.188.251'),
(307, 'Failed Login attempt: not found eb6ebe6105989f5f2ab85c092b5aab27', '2021-07-23 17:03:51', 0, '5.71.103.228'),
(308, 'Loged user pofenas', '2021-07-23 17:03:57', 1, '5.71.103.228'),
(309, 'Loged user pofenas', '2021-07-23 17:06:45', 1, '5.71.103.228'),
(310, 'Loged user pofenas', '2021-07-23 17:09:01', 1, '5.71.103.228'),
(311, 'Failed autoLogin attempt: no token provided', '2021-07-23 17:16:08', 0, '5.71.103.228'),
(312, 'Failed autoLogin attempt: no token provided', '2021-07-23 17:16:35', 0, '5.71.103.228'),
(313, 'Loged user pofenas', '2021-07-23 18:07:54', 1, '5.71.103.228'),
(314, 'Failed Login attempt: not found 77649b504e4ce1d8b6966f8a595bd975', '2021-07-23 18:37:45', 0, '5.71.103.228'),
(315, 'Loged user pofenas', '2021-07-23 18:37:52', 1, '5.71.103.228'),
(316, 'Loged user pofenas', '2021-07-23 19:23:11', 1, '5.71.103.228'),
(317, 'Loged user pofenas', '2021-07-23 19:30:18', 1, '5.71.103.228'),
(318, 'Loged user pofenas', '2021-07-23 19:37:17', 1, '5.71.103.228'),
(319, 'Loged user pofenas', '2021-07-24 15:38:37', 1, '5.71.103.228'),
(320, 'Failed Login attempt: not found 93b885adfe0da089cdf634904fd59f71', '2021-07-24 16:54:36', 0, '84.126.188.251'),
(321, 'Failed Login attempt: not found 93b885adfe0da089cdf634904fd59f71', '2021-07-24 17:00:53', 0, '84.126.188.251'),
(322, 'Failed Login attempt: not found 93b885adfe0da089cdf634904fd59f71', '2021-07-24 17:08:15', 0, '84.126.188.251'),
(323, 'Failed autoLogin attempt: no token provided', '2021-07-24 17:26:16', 0, '84.126.188.251'),
(324, 'Failed autoLogin attempt: no token provided', '2021-07-24 17:27:19', 0, '84.126.188.251'),
(325, 'Failed autoLogin attempt: no token provided', '2021-07-24 17:28:42', 0, '84.126.188.251'),
(326, 'Failed autoLogin attempt: no token provided', '2021-07-24 17:28:44', 0, '84.126.188.251'),
(327, 'Failed autoLogin attempt: no token provided', '2021-07-24 17:28:45', 0, '84.126.188.251'),
(328, 'Failed autoLogin attempt: no token provided', '2021-07-24 17:28:51', 0, '84.126.188.251'),
(329, 'Failed autoLogin attempt: no token provided', '2021-07-24 17:28:57', 0, '84.126.188.251'),
(330, 'Failed autoLogin attempt: no token provided', '2021-07-24 17:30:56', 0, '84.126.188.251'),
(331, 'Failed autoLogin attempt: no token provided', '2021-07-24 17:31:03', 0, '84.126.188.251'),
(332, 'Failed autoLogin attempt: no token provided', '2021-07-24 17:31:13', 0, '84.126.188.251'),
(333, 'Failed autoLogin attempt: no token provided', '2021-07-24 17:31:26', 0, '84.126.188.251'),
(334, 'Failed autoLogin attempt: no token provided', '2021-07-24 17:31:47', 0, '84.126.188.251'),
(335, 'Failed autoLogin attempt: no token provided', '2021-07-24 17:32:41', 0, '84.126.188.251'),
(336, 'Failed autoLogin attempt: no token provided', '2021-07-24 17:33:10', 0, '84.126.188.251'),
(337, 'Failed autoLogin attempt: no token provided', '2021-07-24 17:33:54', 0, '84.126.188.251'),
(338, 'Loged user pofenas', '2021-07-24 19:14:33', 1, '5.71.103.228'),
(339, 'Failed Login attempt: not found 93b885adfe0da089cdf634904fd59f71', '2021-07-25 08:52:00', 0, '84.126.188.251'),
(340, 'Failed Login attempt: not found 93b885adfe0da089cdf634904fd59f71', '2021-07-25 09:05:43', 0, '84.126.188.251'),
(341, 'Failed autoLogin attempt: no token provided', '2021-07-25 09:16:13', 0, '84.126.188.251'),
(342, 'Failed autoLogin attempt: no token provided', '2021-07-25 09:19:43', 0, '84.126.188.251'),
(343, 'Failed Login attempt: not found 93b885adfe0da089cdf634904fd59f71', '2021-07-25 11:24:08', 0, '84.126.188.251'),
(344, 'Failed autoLogin attempt: no token provided', '2021-07-25 11:24:23', 0, '84.126.188.251'),
(345, 'Failed autoLogin attempt: no token provided', '2021-07-25 15:10:45', 0, '84.126.188.251'),
(346, 'Failed autoLogin attempt: no token provided', '2021-07-25 15:13:12', 0, '84.126.188.251'),
(347, 'Failed autoLogin attempt: no token provided', '2021-07-25 15:15:48', 0, '84.126.188.251'),
(348, 'Failed autoLogin attempt: no token provided', '2021-07-25 15:17:47', 0, '84.126.188.251'),
(349, 'Failed autoLogin attempt: no token provided', '2021-07-25 15:18:35', 0, '84.126.188.251'),
(350, 'Failed autoLogin attempt: no token provided', '2021-07-25 15:19:36', 0, '84.126.188.251'),
(351, 'Failed autoLogin attempt: no token provided', '2021-07-25 15:23:04', 0, '84.126.188.251'),
(352, 'Failed autoLogin attempt: no token provided', '2021-07-25 15:23:08', 0, '84.126.188.251'),
(353, 'Failed Login attempt: not found 93b885adfe0da089cdf634904fd59f71', '2021-07-25 15:23:32', 0, '84.126.188.251'),
(354, 'Failed autoLogin attempt: no token provided', '2021-07-25 15:23:36', 0, '84.126.188.251'),
(355, 'Failed Login attempt: not found 93b885adfe0da089cdf634904fd59f71', '2021-07-25 15:23:40', 0, '84.126.188.251'),
(356, 'Failed autoLogin attempt: no token provided', '2021-07-25 15:30:03', 0, '84.126.188.251'),
(357, 'Failed autoLogin attempt: no token provided', '2021-07-25 15:30:08', 0, '84.126.188.251'),
(358, 'Failed autoLogin attempt: no token provided', '2021-07-25 15:32:23', 0, '84.126.188.251'),
(359, 'Failed autoLogin attempt: no token provided', '2021-07-25 15:36:35', 0, '84.126.188.251'),
(360, 'Failed autoLogin attempt: no token provided', '2021-07-25 15:37:00', 0, '84.126.188.251'),
(361, 'Failed autoLogin attempt: no token provided', '2021-07-25 15:37:23', 0, '84.126.188.251'),
(362, 'Loged user pofenas', '2021-07-25 15:40:18', 1, '5.71.103.228'),
(363, 'Failed autoLogin attempt: no token provided', '2021-07-25 15:40:26', 0, '84.126.188.251'),
(364, 'Failed autoLogin attempt: no token provided', '2021-07-25 15:46:09', 0, '84.126.188.251'),
(365, 'Failed autoLogin attempt: no token provided', '2021-07-25 15:46:39', 0, '84.126.188.251'),
(366, 'Loged user pofenas', '2021-07-25 15:54:56', 1, '5.71.103.228'),
(367, 'Loged user pofenas', '2021-07-26 09:23:26', 1, '149.241.194.102'),
(368, 'Failed Login attempt: not found a160a547f7e7f3d37302e17ddaf0c0ae', '2021-07-26 09:25:30', 0, '149.241.194.102'),
(369, 'Failed autoLogin attempt: no token provided', '2021-07-26 09:25:38', 0, '149.241.194.102'),
(370, 'Failed autoLogin attempt: no token provided', '2021-07-26 09:41:02', 0, '84.126.188.251'),
(371, 'Failed autoLogin attempt: no token provided', '2021-07-26 09:42:15', 0, '84.126.188.251'),
(372, 'Failed Login attempt: not found 93b885adfe0da089cdf634904fd59f71', '2021-07-26 10:48:04', 0, '84.126.188.251'),
(373, 'Loged user pofenas', '2021-07-26 10:48:15', 1, '84.126.188.251'),
(374, 'Failed Login attempt: not found a160a547f7e7f3d37302e17ddaf0c0ae', '2021-07-26 10:52:12', 0, '149.241.194.102'),
(375, 'Loged user pofenas', '2021-07-26 10:52:23', 1, '149.241.194.102'),
(376, 'Loged user pofenas', '2021-07-26 15:23:24', 1, '149.241.194.102'),
(377, 'Loged user pofenas', '2021-07-26 15:26:49', 1, '149.241.194.102'),
(378, 'Failed Login attempt: not found a160a547f7e7f3d37302e17ddaf0c0ae', '2021-07-26 19:53:30', 0, '149.241.194.102'),
(379, 'Loged user pofenas', '2021-07-26 19:53:35', 1, '149.241.194.102'),
(380, 'Loged user pofenas', '2021-07-26 19:55:56', 1, '149.241.194.102'),
(381, 'Loged user pofenas', '2021-07-26 20:08:03', 1, '149.241.194.102'),
(382, 'Failed autoLogin attempt: no token provided', '2021-07-26 20:24:06', 0, '149.241.194.102'),
(383, 'Failed autoLogin attempt: no token provided', '2021-07-26 20:24:23', 0, '149.241.194.102'),
(384, 'Loged user pofenas', '2021-07-26 20:32:21', 1, '149.241.194.102'),
(385, 'Failed Login attempt: not found 93b885adfe0da089cdf634904fd59f71', '2021-07-27 09:21:24', 0, '84.126.188.251'),
(386, 'Loged user pofenas', '2021-07-27 09:21:37', 1, '84.126.188.251'),
(387, 'Loged user pofenas', '2021-07-27 17:14:54', 1, '149.241.194.102'),
(388, 'Loged user pofenas', '2021-07-27 17:41:22', 1, '149.241.194.102'),
(389, 'Loged user pofenas', '2021-07-27 20:13:14', 1, '149.241.194.102'),
(390, 'Failed Login attempt: no token provided', '2021-08-02 16:47:39', 0, '185.212.136.117'),
(391, 'Loged user pofenas', '2021-08-02 16:52:15', 1, '149.241.194.102'),
(392, 'Pedro Perez Pofenas2: new user registration.', '2021-08-04 17:27:23', 0, '127.0.0.1'),
(393, 'Pedro Perez Pofenas22: new user registration.', '2021-08-05 08:16:27', 0, '127.0.0.1'),
(394, 'Loged user pofenas', '2021-08-10 16:23:54', 1, '149.241.194.102'),
(395, 'Loged user pofenas', '2021-08-14 14:41:18', 1, '149.241.194.102'),
(396, 'Loged user pofenas', '2021-08-14 14:48:01', 1, '149.241.194.102'),
(397, 'Failed Login attempt: not found 93b885adfe0da089cdf634904fd59f71', '2021-08-23 16:32:16', 0, '84.126.188.251'),
(398, 'Loged user pofenas', '2021-08-23 16:32:21', 1, '84.126.188.251'),
(399, 'Loged user pofenas', '2021-09-01 16:55:16', 1, '185.212.136.117'),
(400, 'Loged user pofenas', '2021-09-02 09:43:59', 1, '185.212.136.117'),
(401, 'Failed Login attempt: no token provided', '2021-11-17 13:33:33', 0, '51.19.75.35'),
(402, 'Logout user <not loged>', '2021-11-17 15:15:57', 0, '51.19.75.35'),
(403, 'Failed Login attempt: not found 93b885adfe0da089cdf634904fd59f71', '2021-11-17 16:07:37', 0, '80.52.117.108'),
(404, 'Failed Login attempt: no token provided', '2021-11-17 19:23:20', 1, '80.52.114.109'),
(405, 'Failed Login attempt: no token provided', '2021-11-17 19:33:47', 23, '80.52.114.109'),
(406, 'Loged user pofenas', '2021-11-19 10:50:42', 1, '80.52.117.108'),
(407, 'Loged user pofenas', '2021-11-19 11:00:31', 1, '80.52.117.108'),
(408, 'Loged user pofenas', '2021-11-19 11:02:58', 1, '80.52.117.108'),
(409, 'Failed autoLogin attempt: no token provided', '2021-11-19 11:04:28', 0, '80.52.117.108'),
(410, 'Failed autoLogin attempt: no token provided', '2021-11-19 11:04:35', 0, '80.52.117.108'),
(411, 'Failed autoLogin attempt: no token provided', '2021-11-19 11:04:40', 0, '80.52.117.108'),
(412, 'Failed autoLogin attempt: no token provided', '2021-11-19 11:04:53', 0, '80.52.117.108'),
(413, 'Failed autoLogin attempt: no token provided', '2021-11-19 11:05:20', 0, '80.52.117.108'),
(414, 'Failed autoLogin attempt: no token provided', '2021-11-19 11:06:34', 0, '80.52.117.108'),
(415, 'Loged user pofenas', '2021-11-22 16:15:11', 1, '80.52.117.108'),
(416, 'Failed autoLogin attempt: no token provided', '2021-11-22 16:18:29', 0, '80.52.117.108'),
(417, 'Failed autoLogin attempt: no token provided', '2021-11-22 16:18:42', 0, '80.52.117.108'),
(418, 'Failed autoLogin attempt: no token provided', '2021-11-22 16:19:10', 0, '80.52.117.108'),
(419, 'Failed autoLogin attempt: no token provided', '2021-11-22 16:19:56', 0, '80.52.117.108'),
(420, 'Failed autoLogin attempt: no token provided', '2021-11-22 16:20:11', 0, '80.52.117.108'),
(421, 'Failed autoLogin attempt: no token provided', '2021-11-22 16:20:24', 0, '80.52.117.108'),
(422, 'Failed autoLogin attempt: no token provided', '2021-11-22 16:20:26', 0, '80.52.117.108'),
(423, 'Failed autoLogin attempt: no token provided', '2021-11-22 16:20:28', 0, '80.52.117.108'),
(424, 'Failed autoLogin attempt: no token provided', '2021-11-22 16:20:30', 0, '80.52.117.108'),
(425, 'Failed autoLogin attempt: no token provided', '2021-11-22 16:20:33', 0, '80.52.117.108'),
(426, 'Loged user pofenas', '2021-11-22 16:20:38', 1, '80.52.117.108'),
(427, 'hola@irma: new user registration.', '2021-11-22 16:31:17', 0, '80.52.117.108'),
(428, 'Loged user pofenas', '2021-11-22 17:12:17', 1, '80.52.117.108'),
(429, 'Failed autoLogin attempt: no token provided', '2021-11-22 17:15:52', 0, '80.52.117.108'),
(430, 'Loged user pofenas', '2021-11-22 17:16:17', 1, '80.52.117.108'),
(431, 'Failed autoLogin attempt: no token provided', '2021-11-22 17:20:16', 0, '80.52.117.108'),
(432, ': new user registration.', '2021-11-23 17:40:27', 0, '80.52.117.108'),
(433, 'Irma: new user registration.', '2021-11-23 17:49:31', 0, '80.52.117.108'),
(434, 'Failed Login attempt: not found 4245af7140b0d3cf62c9db6066465e55', '2021-11-23 17:51:10', 0, '80.52.117.108'),
(435, 'Failed Login attempt: not found db5eaee1da6d236f895349068cd98b10', '2021-11-23 18:01:59', 0, '80.52.117.108'),
(436, 'Failed Login attempt: no token provided', '2021-11-23 18:02:14', 0, '80.52.117.108'),
(437, 'Failed Login attempt: not found ea4824cf4c59bb9d076f6deaf918d289', '2021-11-23 18:02:59', 0, '80.52.117.108'),
(438, 'irma: new user registration.', '2021-11-23 18:53:36', 0, '80.52.117.108'),
(439, 'Loged user pofenas', '2021-11-24 15:41:51', 1, '80.52.117.108'),
(440, 'Failed autoLogin attempt: no token provided', '2021-11-24 15:45:44', 0, '80.52.117.108'),
(441, 'Loged user pofenas', '2021-11-24 15:45:50', 1, '80.52.117.108'),
(442, 'Loged user pofenas', '2021-11-24 15:47:08', 1, '80.52.117.108'),
(443, 'Loged user pofenas', '2021-11-25 09:09:15', 1, '80.52.117.108'),
(444, 'Failed autoLogin attempt: no token provided', '2021-11-25 09:13:24', 0, '80.52.117.108'),
(445, 'Loged user pofenas', '2021-11-25 09:13:44', 1, '80.52.117.108'),
(446, 'Failed autoLogin attempt: no token provided', '2021-11-25 09:26:42', 0, '80.52.117.108'),
(447, 'Failed autoLogin attempt: no token provided', '2021-11-25 09:26:49', 0, '80.52.117.108'),
(448, 'Failed autoLogin attempt: no token provided', '2021-11-25 09:28:17', 0, '80.52.117.108'),
(449, 'Loged user pofenas', '2021-11-25 09:28:23', 1, '80.52.117.108'),
(450, 'Failed autoLogin attempt: no token provided', '2021-11-25 09:29:42', 0, '80.52.117.108'),
(451, 'Failed autoLogin attempt: no token provided', '2021-11-25 09:30:05', 0, '80.52.117.108'),
(452, 'Failed autoLogin attempt: no token provided', '2021-11-25 09:30:13', 0, '80.52.117.108'),
(453, 'Loged user pofenas', '2021-11-25 09:30:43', 1, '80.52.117.108'),
(454, 'Failed Login attempt: not found 93b885adfe0da089cdf634904fd59f71', '2021-11-29 11:43:15', 0, '80.52.117.108'),
(455, 'Failed Login attempt: not found d41d8cd98f00b204e9800998ecf8427e', '2021-11-29 11:43:18', 0, '80.52.117.108'),
(456, 'Failed Login attempt: not found d41d8cd98f00b204e9800998ecf8427e', '2021-11-29 11:43:19', 0, '80.52.117.108'),
(457, 'Failed Login attempt: not found 51005f4784c27fefe56f3bfbd7dd899e', '2021-11-29 22:30:10', 0, '80.52.117.108'),
(458, 'Loged user pofenas', '2021-11-30 16:14:57', 1, '80.52.117.108'),
(459, 'Failed Login attempt: not found 93b885adfe0da089cdf634904fd59f71', '2021-11-30 16:33:44', 0, '80.52.117.108'),
(460, 'Failed autoLogin attempt: no token provided', '2021-12-07 08:28:48', 0, '80.52.117.108'),
(461, 'Failed autoLogin attempt: no token provided', '2021-12-07 08:40:21', 0, '80.52.117.108'),
(462, 'Failed autoLogin attempt: no token provided', '2021-12-07 08:40:23', 0, '80.52.117.108'),
(463, 'Failed autoLogin attempt: no token provided', '2021-12-07 08:41:00', 0, '80.52.117.108'),
(464, 'Failed autoLogin attempt: no token provided', '2021-12-07 08:41:28', 0, '80.52.117.108'),
(465, 'Failed autoLogin attempt: no token provided', '2021-12-07 08:41:34', 0, '80.52.117.108'),
(466, 'Failed autoLogin attempt: no token provided', '2021-12-07 08:41:35', 0, '80.52.117.108'),
(467, 'Failed autoLogin attempt: no token provided', '2021-12-07 08:41:40', 0, '80.52.117.108'),
(468, 'Failed autoLogin attempt: no token provided', '2021-12-07 08:41:42', 0, '80.52.117.108'),
(469, 'Failed autoLogin attempt: no token provided', '2021-12-07 08:42:53', 0, '80.52.117.108'),
(470, 'Failed autoLogin attempt: no token provided', '2021-12-07 08:44:40', 0, '80.52.117.108'),
(471, 'Failed autoLogin attempt: no token provided', '2021-12-07 08:44:46', 0, '80.52.117.108'),
(472, 'Failed autoLogin attempt: no token provided', '2021-12-07 08:45:08', 0, '80.52.117.108'),
(473, 'Failed autoLogin attempt: no token provided', '2021-12-07 08:45:14', 0, '80.52.117.108'),
(474, 'Failed autoLogin attempt: no token provided', '2021-12-07 08:47:06', 0, '80.52.117.108'),
(475, 'Failed autoLogin attempt: no token provided', '2021-12-07 08:52:02', 0, '80.52.117.108'),
(476, 'Failed autoLogin attempt: no token provided', '2021-12-07 08:52:56', 0, '80.52.117.108'),
(477, 'Failed autoLogin attempt: no token provided', '2021-12-07 08:53:00', 0, '80.52.117.108'),
(478, 'Failed autoLogin attempt: no token provided', '2021-12-07 08:53:10', 0, '80.52.117.108'),
(479, 'Failed autoLogin attempt: no token provided', '2021-12-07 08:53:20', 0, '80.52.117.108'),
(480, 'Failed autoLogin attempt: no token provided', '2021-12-07 08:53:24', 0, '80.52.117.108'),
(481, 'Failed autoLogin attempt: no token provided', '2021-12-07 08:53:26', 0, '80.52.117.108'),
(482, 'Failed autoLogin attempt: no token provided', '2021-12-07 08:53:30', 0, '80.52.117.108'),
(483, 'Failed autoLogin attempt: no token provided', '2021-12-07 08:53:31', 0, '80.52.117.108'),
(484, 'Failed autoLogin attempt: no token provided', '2021-12-07 10:32:19', 0, '80.52.117.108'),
(485, 'Failed autoLogin attempt: no token provided', '2021-12-07 11:39:47', 0, '80.52.117.108'),
(486, 'Failed autoLogin attempt: no token provided', '2021-12-07 11:41:16', 0, '80.52.117.108'),
(487, 'Loged user pofenas', '2021-12-09 16:51:31', 1, '80.52.114.109'),
(488, 'Loged user pofenas', '2021-12-09 16:51:51', 1, '80.52.114.109'),
(489, 'Loged user pofenas', '2021-12-15 07:23:51', 1, '80.52.117.108'),
(490, 'Failed autoLogin attempt: no token provided', '2021-12-16 18:57:35', 0, '80.52.117.108'),
(491, 'Loged user pofenas', '2022-02-04 07:39:24', 1, '80.52.114.109'),
(492, 'Failed autoLogin attempt: no token provided', '2022-03-26 14:09:41', 0, '136.56.167.134'),
(493, 'Failed Login attempt: no token provided', '2022-08-27 16:43:39', 0, '94.194.172.73'),
(494, 'Failed Login attempt: no token provided', '2022-08-27 16:44:30', 0, '94.194.172.73'),
(495, 'Failed Login attempt: no token provided', '2022-08-27 16:45:06', 0, '94.194.172.73'),
(496, 'Failed Login attempt: no token provided', '2022-08-27 16:46:35', 0, '94.194.172.73'),
(497, 'Failed Login attempt: no token provided', '2022-08-27 16:48:32', 0, '94.194.172.73'),
(498, 'Failed Login attempt: no token provided', '2022-08-27 16:48:53', 0, '94.194.172.73'),
(499, 'Failed Login attempt: no token provided', '2022-08-27 16:58:18', 0, '94.194.172.73'),
(500, 'Failed Login attempt: no token provided', '2022-08-27 16:59:30', 0, '94.194.172.73'),
(501, 'Failed Login attempt: no token provided', '2022-08-27 17:00:19', 0, '94.194.172.73'),
(502, 'Failed Login attempt: no token provided', '2022-08-27 17:01:53', 0, '94.194.172.73'),
(503, 'Failed Login attempt: no token provided', '2022-08-27 17:03:15', 0, '94.194.172.73'),
(504, 'Failed Login attempt: no token provided', '2022-08-27 17:03:44', 0, '94.194.172.73'),
(505, 'Failed autoLogin attempt: no token provided', '2022-12-19 15:49:12', 0, '127.0.0.1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `GLOBAL_PARMS`
--
ALTER TABLE `GLOBAL_PARMS`
  ADD PRIMARY KEY (`parm`);

--
-- Indexes for table `LANDING`
--
ALTER TABLE `LANDING`
  ADD PRIMARY KEY (`id_landing`);

--
-- Indexes for table `LANGUAGES`
--
ALTER TABLE `LANGUAGES`
  ADD PRIMARY KEY (`id_language`);

--
-- Indexes for table `PLANS`
--
ALTER TABLE `PLANS`
  ADD PRIMARY KEY (`id_plan`);

--
-- Indexes for table `REL_USERS_CATEGORIES`
--
ALTER TABLE `REL_USERS_CATEGORIES`
  ADD PRIMARY KEY (`id_ruc`),
  ADD KEY `id_category` (`id_category`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `REL_USERS_PLANS`
--
ALTER TABLE `REL_USERS_PLANS`
  ADD PRIMARY KEY (`id_rup`);

--
-- Indexes for table `USERS`
--
ALTER TABLE `USERS`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `USERS_CATEGORIES`
--
ALTER TABLE `USERS_CATEGORIES`
  ADD PRIMARY KEY (`id_user_category`);

--
-- Indexes for table `USER_LOG`
--
ALTER TABLE `USER_LOG`
  ADD PRIMARY KEY (`id_user_log`),
  ADD KEY `id_user` (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `LANDING`
--
ALTER TABLE `LANDING`
  MODIFY `id_landing` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `PLANS`
--
ALTER TABLE `PLANS`
  MODIFY `id_plan` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `REL_USERS_CATEGORIES`
--
ALTER TABLE `REL_USERS_CATEGORIES`
  MODIFY `id_ruc` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `REL_USERS_PLANS`
--
ALTER TABLE `REL_USERS_PLANS`
  MODIFY `id_rup` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `USERS`
--
ALTER TABLE `USERS`
  MODIFY `id_user` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `USERS_CATEGORIES`
--
ALTER TABLE `USERS_CATEGORIES`
  MODIFY `id_user_category` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `USER_LOG`
--
ALTER TABLE `USER_LOG`
  MODIFY `id_user_log` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=506;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
